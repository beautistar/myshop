DROP TABLE IF EXISTS `almacen`;

CREATE TABLE `almacen` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `almacen_change`;

CREATE TABLE `almacen_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT ,
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `almacen_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Table structure for table `almacenproducto`
--

DROP TABLE IF EXISTS `almacenproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacenproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CostoSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PuntoDeReorden` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `InventarioOptimo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `AlmacenID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_AlmacenID` (`AlmacenID`),
  CONSTRAINT `almacenproducto_ibfk_1` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `almacenproducto_ibfk_2` FOREIGN KEY (`AlmacenID`) REFERENCES `almacen` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `almacenproducto_change`
--

DROP TABLE IF EXISTS `almacenproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacenproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CostoSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PuntoDeReorden` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_InventarioOptimo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_AlmacenID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `almacenproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apievent`
--

DROP TABLE IF EXISTS `apievent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apievent` (
  `Type` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'AFTER',
  `HTTPMethod` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'POST',
  `DataTypeID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Description` longtext COLLATE utf8_unicode_ci,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Code` longtext COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Type` (`Type`),
  KEY `IX_HTTPMethod` (`HTTPMethod`),
  KEY `IX_DataTypeID` (`DataTypeID`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `apievent_ibfk_1` FOREIGN KEY (`DataTypeID`) REFERENCES `datatype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `apievent_ibfk_2` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apievent_change`
--

DROP TABLE IF EXISTS `apievent_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apievent_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Type` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_HTTPMethod` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DataTypeID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Code` longtext COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `apievent_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apimethod`
--

DROP TABLE IF EXISTS `apimethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apimethod` (
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '/api/method/',
  `HTTPMethod` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'GET',
  `RunAsID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_Name` (`Name`),
  KEY `IX_Path` (`Path`),
  KEY `IX_HTTPMethod` (`HTTPMethod`),
  KEY `IX_RunAsID` (`RunAsID`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `apimethod_ibfk_1` FOREIGN KEY (`RunAsID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `apimethod_ibfk_2` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apimethod_change`
--

DROP TABLE IF EXISTS `apimethod_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apimethod_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_HTTPMethod` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Path` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_RunAsID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Code` longtext COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `apimethod_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apirequest`
--

DROP TABLE IF EXISTS `apirequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apirequest` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalogodeproductos`
--

DROP TABLE IF EXISTS `catalogodeproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogodeproductos` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `VigenciaDesde` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `FechaDeUltimaModificacion` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `ConfiguracionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `diassemana` int(11) NOT NULL DEFAULT '0',
  `DiasHorario` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `VigenciaHasta` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `fechaderegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `TipoDeVigencia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_ConfiguracionID` (`ConfiguracionID`),
  CONSTRAINT `catalogodeproductos_ibfk_1` FOREIGN KEY (`ConfiguracionID`) REFERENCES `configuraciondecaja` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalogodeproductos_change`
--

DROP TABLE IF EXISTS `catalogodeproductos_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogodeproductos_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VigenciaDesde` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_FechaDeUltimaModificacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ConfiguracionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_diassemana` int(11) NOT NULL DEFAULT '0',
  `Obj_DiasHorario` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VigenciaHasta` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_fechaderegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TipoDeVigencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `catalogodeproductos_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalogodeproductosproducto`
--

DROP TABLE IF EXISTS `catalogodeproductosproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogodeproductosproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CatalogoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PrecioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CatalogoID` (`CatalogoID`),
  KEY `IX_ProductoID` (`ProductoID`),
  CONSTRAINT `catalogodeproductosproducto_ibfk_1` FOREIGN KEY (`CatalogoID`) REFERENCES `catalogodeproductos` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `catalogodeproductosproducto_ibfk_2` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catalogodeproductosproducto_change`
--

DROP TABLE IF EXISTS `catalogodeproductosproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogodeproductosproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CatalogoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PrecioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `catalogodeproductosproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categoriadeproducto`
--

DROP TABLE IF EXISTS `categoriadeproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoriadeproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CategoriaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Orden` int(11) NOT NULL DEFAULT '0',
  `Color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#8b008b',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Activa` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `IX_CategoriaID` (`CategoriaID`),
  CONSTRAINT `categoriadeproducto_ibfk_1` FOREIGN KEY (`CategoriaID`) REFERENCES `categoriadeproducto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categoriadeproducto_change`
--

DROP TABLE IF EXISTS `categoriadeproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoriadeproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CategoriaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Orden` int(11) NOT NULL DEFAULT '0',
  `Obj_Colonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Activa` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `categoriadeproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `certificado`
--

DROP TABLE IF EXISTS `certificado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificado` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `LlavePublica` longblob,
  `EmpresaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `LlavePrivada` longblob,
  `LlavePrivadaFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_EmpresaID` (`EmpresaID`),
  CONSTRAINT `certificado_ibfk_1` FOREIGN KEY (`EmpresaID`) REFERENCES `empresa` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `certificado_change`
--

DROP TABLE IF EXISTS `certificado_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificado_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LlavePublica` longblob,
  `Obj_EmpresaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LlavePrivada` longblob,
  `Obj_LlavePrivadaFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `certificado_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacion1`
--

DROP TABLE IF EXISTS `clasificacion1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion1` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacion1_change`
--

DROP TABLE IF EXISTS `clasificacion1_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion1_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacion1_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacion2`
--

DROP TABLE IF EXISTS `clasificacion2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion2` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clasificacion2` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacion2_change`
--

DROP TABLE IF EXISTS `clasificacion2_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion2_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacion2_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacion3`
--

DROP TABLE IF EXISTS `clasificacion3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion3` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacion3_change`
--

DROP TABLE IF EXISTS `clasificacion3_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion3_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacion3_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacioncliente1`
--

DROP TABLE IF EXISTS `clasificacioncliente1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacioncliente1` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacioncliente1_change`
--

DROP TABLE IF EXISTS `clasificacioncliente1_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacioncliente1_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacioncliente1_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacioncliente2`
--

DROP TABLE IF EXISTS `clasificacioncliente2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacioncliente2` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacioncliente2_change`
--

DROP TABLE IF EXISTS `clasificacioncliente2_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacioncliente2_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacioncliente2_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacioncliente3`
--

DROP TABLE IF EXISTS `clasificacioncliente3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacioncliente3` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacioncliente3_change`
--

DROP TABLE IF EXISTS `clasificacioncliente3_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacioncliente3_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacioncliente3_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacionempleado1`
--

DROP TABLE IF EXISTS `clasificacionempleado1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacionempleado1` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacionempleado1_change`
--

DROP TABLE IF EXISTS `clasificacionempleado1_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacionempleado1_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacionempleado1_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacionempleado2`
--

DROP TABLE IF EXISTS `clasificacionempleado2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacionempleado2` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacionempleado2_change`
--

DROP TABLE IF EXISTS `clasificacionempleado2_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacionempleado2_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacionempleado2_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacionempleado3`
--

DROP TABLE IF EXISTS `clasificacionempleado3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacionempleado3` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clasificacionempleado3_change`
--

DROP TABLE IF EXISTS `clasificacionempleado3_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacionempleado3_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clasificacionempleado3_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `FechaDeNacimiento` datetime(6) NOT NULL DEFAULT '1900-01-01 00:00:00.000000',
  `Apellidos` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LimiteDeCredito` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  `Saldo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NotificarSaldoMenorA` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Telefono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PorcentajeDeDescuento` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `AplicarDescuento` tinyint(1) NOT NULL DEFAULT '0',
  `Clasificacion2ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Fax` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Celular` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PaginaWeb` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `NumeroDeCliente` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clasificacion1ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Clasificacion3ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `IdentificadorFiscal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NombreFiscal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `EmpresaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `InformacionAdicional` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Activo',
  `DivisaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PlazoDePagoDias` int(11) NOT NULL DEFAULT '0',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegistradoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Vendedor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaDeRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `RegistradoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `VendedorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Email` (`Email`),
  KEY `IX_Apellidos` (`Apellidos`),
  KEY `IX_Nombre` (`Nombre`),
  KEY `IX_Password` (`Password`),
  KEY `IX_Email` (`Email`),
  KEY `IX_Clasificacion2ID` (`Clasificacion2ID`),
  KEY `IX_TipoID` (`TipoID`),
  KEY `IX_Clasificacion1ID` (`Clasificacion1ID`),
  KEY `IX_Clasificacion3ID` (`Clasificacion3ID`),
  KEY `IX_EmpresaID` (`EmpresaID`),
  KEY `IX_DivisaID` (`DivisaID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_NumeroDeCliente` (`NumeroDeCliente`),
  KEY `IX_RegistradoPorID` (`RegistradoPorID`),
  KEY `IX_VendedorID` (`VendedorID`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`Clasificacion2ID`) REFERENCES `clasificacioncliente2` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`TipoID`) REFERENCES `tipodecliente` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`Clasificacion1ID`) REFERENCES `clasificacioncliente1` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_4` FOREIGN KEY (`Clasificacion3ID`) REFERENCES `clasificacioncliente3` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_5` FOREIGN KEY (`EmpresaID`) REFERENCES `empresa` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_6` FOREIGN KEY (`DivisaID`) REFERENCES `divisa` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_7` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_8` FOREIGN KEY (`RegistradoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cliente_ibfk_9` FOREIGN KEY (`VendedorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente_change`
--

DROP TABLE IF EXISTS `cliente_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaDeNacimiento` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_PorcentajeDeDescuento` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Apellidos` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LimiteDeCredito` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_AplicarDescuento` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Clasificacion2ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia3` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Activo` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Saldo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Celular` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaDeRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_PaginaWeb` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EmpresaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NotificarSaldoMenorA` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeCliente` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegistradoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InformacionAdicional` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PlazoDePagoDias` int(11) NOT NULL DEFAULT '0',
  `Obj_Vendedor` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clasificacion1ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clasificacion3ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estatus` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DivisaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_IdentificadorFiscal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreFiscal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VendedorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `cliente_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientecatalogodeproductos`
--

DROP TABLE IF EXISTS `clientecatalogodeproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientecatalogodeproductos` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CatalogoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CatalogoID` (`CatalogoID`),
  KEY `IX_ClienteID` (`ClienteID`),
  CONSTRAINT `clientecatalogodeproductos_ibfk_1` FOREIGN KEY (`CatalogoID`) REFERENCES `catalogodeproductos` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `clientecatalogodeproductos_ibfk_2` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientecatalogodeproductos_change`
--

DROP TABLE IF EXISTS `clientecatalogodeproductos_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientecatalogodeproductos_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CatalogoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clientecatalogodeproductos_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientecontacto`
--

DROP TABLE IF EXISTS `clientecontacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientecontacto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Cargo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Sexo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'NoEspecificado',
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InformacionAdicional` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Titulo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaDeNacimiento` datetime(6) NOT NULL DEFAULT '1900-01-01 00:00:00.000000',
  `Apellido` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Celular` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `EsPrincipal` tinyint(1) NOT NULL DEFAULT '0',
  `Telefono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_ClienteID` (`ClienteID`),
  CONSTRAINT `clientecontacto_ibfk_1` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientecontacto_change`
--

DROP TABLE IF EXISTS `clientecontacto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientecontacto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cargo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Sexo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InformacionAdicional` longtext COLLATE utf8_unicode_ci,
  `Obj_Titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaDeNacimiento` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Apellido` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Celular` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EsPrincipal` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clientecontacto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientedireccion`
--

DROP TABLE IF EXISTS `clientedireccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientedireccion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `EsDireccionFiscal` tinyint(1) NOT NULL DEFAULT '0',
  `NumeroExterior` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Notas` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `FechaUltimaModificacion` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `EntreCalles` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `CP` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Fax` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroInterior` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Telefono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Pais` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Calle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_TipoID` (`TipoID`),
  KEY `IX_ClienteID` (`ClienteID`),
  CONSTRAINT `clientedireccion_ibfk_1` FOREIGN KEY (`TipoID`) REFERENCES `tipodedireccion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `clientedireccion_ibfk_2` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientedireccion_change`
--

DROP TABLE IF EXISTS `clientedireccion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientedireccion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EsDireccionFiscal` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_NumeroExterior` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Notas` longtext COLLATE utf8_unicode_ci,
  `Obj_FechaUltimaModificacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntreCalles` longtext COLLATE utf8_unicode_ci,
  `Obj_CP` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroInterior` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Pais` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Calle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Colonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clientedireccion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clienteprogramadelealtad`
--

DROP TABLE IF EXISTS `clienteprogramadelealtad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienteprogramadelealtad` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProgramaDeLealtadID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProgramaDeLealtadID` (`ProgramaDeLealtadID`),
  KEY `IX_ClienteID` (`ClienteID`),
  CONSTRAINT `clienteprogramadelealtad_ibfk_1` FOREIGN KEY (`ProgramaDeLealtadID`) REFERENCES `programadelealtad` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `clienteprogramadelealtad_ibfk_2` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clienteprogramadelealtad_change`
--

DROP TABLE IF EXISTS `clienteprogramadelealtad_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienteprogramadelealtad_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProgramaDeLealtadID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `clienteprogramadelealtad_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientesorteo`
--

DROP TABLE IF EXISTS `clientesorteo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientesorteo` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `nombre` varchar(255) DEFAULT '',
  `telefono` varchar(255) DEFAULT 'ssss\nsss',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `codigopostal`
--

DROP TABLE IF EXISTS `codigopostal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigopostal` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ColoniaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `CP` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Municipio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Zona` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciondecaja`
--

DROP TABLE IF EXISTS `configuraciondecaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciondecaja` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FormatoReciboDeNominaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoExistenciaInventarioID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoFacturaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoPedidoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoMovimientoInventarioID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoCFDIID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoTicketID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoRetiroDeCajaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoCorteDeCajaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormatoDevolucionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `AgruparProductosEnTicketYCuenta` tinyint(1) NOT NULL DEFAULT '0',
  `AperturaCajonDeDinero` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'AlPagar',
  `SincronizarVentasEnTerminales` tinyint(1) NOT NULL DEFAULT '0',
  `Impuesto3Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto2Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `RequerirContrasenaParaCerrarTerminalAsistencia` tinyint(1) NOT NULL DEFAULT '0',
  `Autenticacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ClasificacionCliente3Nombre` varchar(255) DEFAULT '',
  `EsDefault` tinyint(1) NOT NULL DEFAULT '0',
  `FormasDePagoAceptadas` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `FormasDePagoDevoluciones` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FormatoReciboDeNominaID` (`FormatoReciboDeNominaID`),
  KEY `IX_FormatoExistenciaInventarioID` (`FormatoExistenciaInventarioID`),
  KEY `IX_FormatoFacturaID` (`FormatoFacturaID`),
  KEY `IX_FormatoPedidoID` (`FormatoPedidoID`),
  KEY `IX_FormatoMovimientoInventarioID` (`FormatoMovimientoInventarioID`),
  KEY `IX_FormatoCFDIID` (`FormatoCFDIID`),
  KEY `IX_FormatoTicketID` (`FormatoTicketID`),
  KEY `IX_FormatoRetiroDeCajaID` (`FormatoRetiroDeCajaID`),
  KEY `IX_FormatoCorteDeCajaID` (`FormatoCorteDeCajaID`),
  KEY `IX_FormatoDevolucionID` (`FormatoDevolucionID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  CONSTRAINT `configuraciondecaja_ibfk_1` FOREIGN KEY (`FormatoReciboDeNominaID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_10` FOREIGN KEY (`FormatoDevolucionID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_11` FOREIGN KEY (`FormaDePagoID`) REFERENCES `configuraciondecajaformadepago` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_2` FOREIGN KEY (`FormatoExistenciaInventarioID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_3` FOREIGN KEY (`FormatoFacturaID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_4` FOREIGN KEY (`FormatoPedidoID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_5` FOREIGN KEY (`FormatoMovimientoInventarioID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_6` FOREIGN KEY (`FormatoCFDIID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_7` FOREIGN KEY (`FormatoTicketID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_8` FOREIGN KEY (`FormatoRetiroDeCajaID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecaja_ibfk_9` FOREIGN KEY (`FormatoCorteDeCajaID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciondecaja_change`
--

DROP TABLE IF EXISTS `configuraciondecaja_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciondecaja_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoReciboDeNominaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoExistenciaInventarioID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Autenticacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AgruparProductosEnTicketYCuenta` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Impuesto3Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FormasDePagoAceptadas` longtext COLLATE utf8_unicode_ci,
  `Obj_FormatoFacturaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoPedidoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormasDePagoDevoluciones` longtext COLLATE utf8_unicode_ci,
  `Obj_Impuesto2Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AperturaCajonDeDinero` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoMovimientoInventarioID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoCFDIID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoTicketID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EsDefault` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_SincronizarVentasEnTerminales` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_FormatoRetiroDeCajaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoCorteDeCajaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto1Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FormatoDevolucionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RequerirContrasenaParaCerrarTerminalAsistencia` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `configuraciondecaja_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciondecajaformadepago`
--

DROP TABLE IF EXISTS `configuraciondecajaformadepago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciondecajaformadepago` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ConfiguracionDeCajaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  KEY `IX_ConfiguracionDeCajaID` (`ConfiguracionDeCajaID`),
  CONSTRAINT `configuraciondecajaformadepago_ibfk_1` FOREIGN KEY (`FormaDePagoID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecajaformadepago_ibfk_2` FOREIGN KEY (`ConfiguracionDeCajaID`) REFERENCES `configuraciondecaja` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciondecajaformadepago_change`
--

DROP TABLE IF EXISTS `configuraciondecajaformadepago_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciondecajaformadepago_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ConfiguracionDeCajaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormaDePagoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `configuraciondecajaformadepago_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciondecajaformadepagodevolucion`
--

DROP TABLE IF EXISTS `configuraciondecajaformadepagodevolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciondecajaformadepagodevolucion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ConfiguracionDeCajaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ConfiguracionDeCajaID` (`ConfiguracionDeCajaID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  CONSTRAINT `configuraciondecajaformadepagodevolucion_ibfk_1` FOREIGN KEY (`ConfiguracionDeCajaID`) REFERENCES `configuraciondecaja` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciondecajaformadepagodevolucion_ibfk_2` FOREIGN KEY (`FormaDePagoID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciondecajaformadepagodevolucion_change`
--

DROP TABLE IF EXISTS `configuraciondecajaformadepagodevolucion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciondecajaformadepagodevolucion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormaDePagoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ConfiguracionDeCajaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `configuraciondecajaformadepagodevolucion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciongeneral`
--

DROP TABLE IF EXISTS `configuraciongeneral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciongeneral` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ReferenciaEmpleado2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ReferenciaCliente3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Referencia',
  `ReferenciaEmpleado1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClasificacionCliente2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ReferenciaEmpleado3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClasificacionEmpleado2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClasificacionCliente1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clasificacion1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Marca',
  `ReferenciaCliente2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'IVA',
  `Clasificacion3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ReferenciaCliente1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CapturarCostosConImpuesto` tinyint(1) NOT NULL DEFAULT '1',
  `ClasificacionEmpleado3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClasificacionEmpleado1Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CapturarPreciosConImpuesto` tinyint(1) NOT NULL DEFAULT '1',
  `Clasificacion2Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Categoría',
  `SegundosAMostrarMensajeDeAsistencia` int(11) NOT NULL DEFAULT '5',
  `SeparadorFolioDeVenta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '-',
  `FormaDePagoCambioID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `LimiteDeCaracteresEnNombreDeProductos` int(11) NOT NULL DEFAULT '15',
  `SegundosParaRecibirNuevoRegistroDeAsistencia` int(11) NOT NULL DEFAULT '60',
  `FormatoCFDIPorInternetID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TipoDeTurnosDeTrabajo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'DiaSolamente',
  `DiasAAlmacenarInformacion` int(11) NOT NULL DEFAULT '0',
  `ClasificacionCliente3Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CodigoDelEstablecimiento` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_FormaDePagoCambioID` (`FormaDePagoCambioID`),
  KEY `IX_FormatoCFDIPorInternetID` (`FormatoCFDIPorInternetID`),
  CONSTRAINT `configuraciongeneral_ibfk_1` FOREIGN KEY (`FormaDePagoCambioID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `configuraciongeneral_ibfk_2` FOREIGN KEY (`FormatoCFDIPorInternetID`) REFERENCES `formatodeimpresion` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuraciongeneral_change`
--

DROP TABLE IF EXISTS `configuraciongeneral_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciongeneral_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SegundosAMostrarMensajeDeAsistencia` int(11) NOT NULL DEFAULT '0',
  `Obj_ReferenciaEmpleado2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SeparadorFolioDeVenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ReferenciaCliente3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClasificacionCliente3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ReferenciaEmpleado1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClasificacionCliente2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormaDePagoCambioID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LimiteDeCaracteresEnNombreDeProductos` int(11) NOT NULL DEFAULT '0',
  `Obj_ReferenciaEmpleado3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClasificacionEmpleado2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClasificacionCliente1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SegundosParaRecibirNuevoRegistroDeAsistencia` int(11) NOT NULL DEFAULT '0',
  `Obj_Clasificacion1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ReferenciaCliente2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clasificacion3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormatoCFDIPorInternetID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeTurnosDeTrabajo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ReferenciaCliente1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CodigoDelEstablecimiento` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DiasAAlmacenarInformacion` int(11) NOT NULL DEFAULT '0',
  `Obj_CapturarCostosConImpuesto` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_ClasificacionEmpleado3Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClasificacionEmpleado1Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CapturarPreciosConImpuesto` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Clasificacion2Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `configuraciongeneral_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortedecaja`
--

DROP TABLE IF EXISTS `cortedecaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortedecaja` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `FolioSecuencial` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FolioUnico` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Depositos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FechaInicial` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Retiros` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `DescuentoImpuesto2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FechaFinal` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Devoluciones` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `RegistradoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TurnoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_RegistradoPorID` (`RegistradoPorID`),
  KEY `IX_TurnoID` (`TurnoID`),
  CONSTRAINT `cortedecaja_ibfk_1` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cortedecaja_ibfk_2` FOREIGN KEY (`RegistradoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cortedecaja_ibfk_3` FOREIGN KEY (`TurnoID`) REFERENCES `turnoterminal` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortedecaja_change`
--

DROP TABLE IF EXISTS `cortedecaja_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortedecaja_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FolioSecuencial` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FolioUnico` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Depositos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FechaInicial` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Retiros` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_DescuentoImpuesto2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FechaFinal` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Devoluciones` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_RegistradoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TurnoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `cortedecaja_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortedecajapago`
--

DROP TABLE IF EXISTS `cortedecajapago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortedecajapago` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `TotalEsperadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CorteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TotalEsperadoMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TotalReportadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TotalDiferenciaMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  KEY `IX_CorteID` (`CorteID`),
  CONSTRAINT `cortedecajapago_ibfk_1` FOREIGN KEY (`FormaDePagoID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cortedecajapago_ibfk_2` FOREIGN KEY (`CorteID`) REFERENCES `cortedecaja` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortedecajapago_change`
--

DROP TABLE IF EXISTS `cortedecajapago_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortedecajapago_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TotalEsperadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FormaDePagoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CorteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TotalEsperadoMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TotalReportadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TotalDiferenciaMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `cortedecajapago_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortedecajaproducto`
--

DROP TABLE IF EXISTS `cortedecajaproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortedecajaproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProductoCantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CorteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeVentaConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVenta2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVenta1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeVentaSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVenta3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_CorteID` (`CorteID`),
  CONSTRAINT `cortedecajaproducto_ibfk_1` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cortedecajaproducto_ibfk_2` FOREIGN KEY (`CorteID`) REFERENCES `cortedecaja` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortedecajaproducto_change`
--

DROP TABLE IF EXISTS `cortedecajaproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortedecajaproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoCantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CorteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeVentaConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVenta2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVenta1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeVentaSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVenta3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `cortedecajaproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cuentabancaria`
--

DROP TABLE IF EXISTS `cuentabancaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentabancaria` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Saldo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_ClienteID` (`ClienteID`),
  CONSTRAINT `cuentabancaria_ibfk_1` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cuentabancaria_change`
--

DROP TABLE IF EXISTS `cuentabancaria_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentabancaria_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `cuentabancaria_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datatype`
--

DROP TABLE IF EXISTS `datatype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datatype` (
  `Identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `LogChanges` tinyint(1) NOT NULL DEFAULT '1',
  `GenerateUIComponent` tinyint(1) NOT NULL DEFAULT '1',
  `BeforeDeleteValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeDeleteValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertOrUpdateValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertOrUpdateValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeUpdateValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeUpdateValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsert` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeUpdate` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeDelete` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AfterInsert` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AfterUpdate` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AfterDelete` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_Name` (`Name`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `datatype_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datatype_change`
--

DROP TABLE IF EXISTS `datatype_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datatype_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_LogChanges` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_GenerateUIComponent` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_BeforeDeleteValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeDeleteValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertOrUpdateValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertOrUpdateValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdateValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdateValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsert` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdate` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeDelete` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterInsert` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterUpdate` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterDelete` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `datatype_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datatypefield`
--

DROP TABLE IF EXISTS `datatypefield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datatypefield` (
  `DataTypeID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'string',
  `DefaultValue` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ReferenceTypeID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `AutoIncrement` tinyint(1) NOT NULL DEFAULT '0',
  `Indexed` tinyint(1) NOT NULL DEFAULT '0',
  `Unique` tinyint(1) NOT NULL DEFAULT '0',
  `StoreHashed` tinyint(1) NOT NULL DEFAULT '0',
  `HashAlgorithm` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SHA512',
  `HashStaticSalt` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Required` tinyint(1) NOT NULL DEFAULT '0',
  `IsNameField` tinyint(1) NOT NULL DEFAULT '0',
  `UIFieldType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'text',
  `UICodeType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'text',
  `UIOrder` bigint(20) NOT NULL DEFAULT '1000',
  `RegularExpression` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `RegularExpressionErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `MinLength` int(11) NOT NULL DEFAULT '0',
  `MaxLength` int(11) NOT NULL DEFAULT '0',
  `GreatherThan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `GreatherOrEqualThan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LessThan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LessOrEqualThan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `AllowedValues` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeDeleteValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeDeleteValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertOrUpdateValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertOrUpdateValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsertValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeUpdateValidation` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeUpdateValidationErrorMessage` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeInsert` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeUpdate` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `BeforeDelete` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AfterInsert` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AfterUpdate` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `AfterDelete` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_DataTypeID` (`DataTypeID`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_Name` (`Name`),
  KEY `IX_Type` (`Type`),
  KEY `IX_ReferenceTypeID` (`ReferenceTypeID`),
  KEY `IX_IsNameField` (`IsNameField`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `datatypefield_ibfk_1` FOREIGN KEY (`DataTypeID`) REFERENCES `datatype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `datatypefield_ibfk_2` FOREIGN KEY (`ReferenceTypeID`) REFERENCES `datatype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `datatypefield_ibfk_3` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datatypefield_change`
--

DROP TABLE IF EXISTS `datatypefield_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datatypefield_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DataTypeID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DefaultValue` longtext COLLATE utf8_unicode_ci,
  `Obj_ReferenceTypeID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AutoIncrement` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Indexed` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Unique` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_StoreHashed` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_HashAlgorithm` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_HashStaticSalt` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Required` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_IsNameField` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_UIFieldType` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UICodeType` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UIOrder` bigint(20) NOT NULL DEFAULT '0',
  `Obj_RegularExpression` longtext COLLATE utf8_unicode_ci,
  `Obj_RegularExpressionErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_MinLength` int(11) NOT NULL DEFAULT '0',
  `Obj_MaxLength` int(11) NOT NULL DEFAULT '0',
  `Obj_GreatherThan` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_GreatherOrEqualThan` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LessThan` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LessOrEqualThan` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AllowedValues` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeDeleteValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeDeleteValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertOrUpdateValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertOrUpdateValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsertValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdateValidation` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdateValidationErrorMessage` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInsert` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdate` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeDelete` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterInsert` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterUpdate` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterDelete` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `datatypefield_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datosreceptorfactura`
--

DROP TABLE IF EXISTS `datosreceptorfactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datosreceptorfactura` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Calle` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `Email` longtext COLLATE utf8_unicode_ci,
  `Colonia` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `CodigoPostal` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `FechaUltimaActualizacion` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Municipio` varchar(120) COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroExterior` varchar(5) COLLATE utf8_unicode_ci DEFAULT '',
  `RFC` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Pais` varchar(120) COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroInterior` varchar(5) COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(120) COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `descuento`
--

DROP TABLE IF EXISTS `descuento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descuento` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductosACobrar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ProductosAOtorgar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `InformacionAdicional` varchar(255) DEFAULT '',
  `DiasHorario` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoDeVigencia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SiempreVigente',
  `DiasSemana` int(11) NOT NULL DEFAULT '0',
  `VigenciaHasta` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `VigenciaDesde` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `ProductosAOtorgarTo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `descuento_change`
--

DROP TABLE IF EXISTS `descuento_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descuento_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DiasHorario` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeVigencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DiasSemana` int(11) NOT NULL DEFAULT '0',
  `Obj_VigenciaHasta` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductosAOtorgar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductosAOtorgarTo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_VigenciaDesde` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `descuento_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devolucion`
--

DROP TABLE IF EXISTS `devolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDeTerminal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `AutorizadaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Motivo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TurnoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Folio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DescuentoPorcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FolioUnico` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SeparadorFolio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '-',
  `CorteDeCajaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoSubtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MontoPagado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FolioSecuencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `VentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Impuesto3PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `EsDevolucion` tinyint(1) NOT NULL DEFAULT '1',
  `RegistradaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Impuesto2PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_AutorizadaPorID` (`AutorizadaPorID`),
  KEY `IX_TurnoID` (`TurnoID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_CorteDeCajaID` (`CorteDeCajaID`),
  KEY `IX_VentaID` (`VentaID`),
  KEY `IX_RegistradaPorID` (`RegistradaPorID`),
  CONSTRAINT `devolucion_ibfk_1` FOREIGN KEY (`AutorizadaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucion_ibfk_2` FOREIGN KEY (`TurnoID`) REFERENCES `turnoterminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucion_ibfk_3` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucion_ibfk_4` FOREIGN KEY (`CorteDeCajaID`) REFERENCES `cortedecaja` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucion_ibfk_5` FOREIGN KEY (`VentaID`) REFERENCES `venta` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucion_ibfk_6` FOREIGN KEY (`RegistradaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devolucion_change`
--

DROP TABLE IF EXISTS `devolucion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AutorizadaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Motivo` longtext COLLATE utf8_unicode_ci,
  `Obj_Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TurnoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoPorcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FolioUnico` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SeparadorFolio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CorteDeCajaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoSubtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MontoPagado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FolioSecuencial` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VentaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Impuesto3PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto1PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_EsDevolucion` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_RegistradaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto2PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `devolucion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devolucionpago`
--

DROP TABLE IF EXISTS `devolucionpago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucionpago` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `FormaDePagoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia` longtext COLLATE utf8_unicode_ci,
  `DevolucionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TipoDeCambio` decimal(26,10) NOT NULL DEFAULT '1.0000000000',
  `MontoPagadoMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `MontoPagadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_DevolucionID` (`DevolucionID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  CONSTRAINT `devolucionpago_ibfk_1` FOREIGN KEY (`DevolucionID`) REFERENCES `devolucion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucionpago_ibfk_2` FOREIGN KEY (`FormaDePagoID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devolucionpago_change`
--

DROP TABLE IF EXISTS `devolucionpago_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucionpago_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormaDePagoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia` longtext COLLATE utf8_unicode_ci,
  `Obj_DevolucionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeCambio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MontoPagadoMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FormaDePagoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MontoPagadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `devolucionpago_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devolucionproducto`
--

DROP TABLE IF EXISTS `devolucionproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucionproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `VentaProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DevolucionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoCodigo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PorcentajeImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoNombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ProductoDevueltoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PrecioDeListaUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ProductoIDID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_VentaProductoID` (`VentaProductoID`),
  KEY `IX_DevolucionID` (`DevolucionID`),
  KEY `IX_ProductoDevueltoID` (`ProductoDevueltoID`),
  KEY `IX_ProductoIDID` (`ProductoIDID`),
  CONSTRAINT `devolucionproducto_ibfk_1` FOREIGN KEY (`VentaProductoID`) REFERENCES `ventaproducto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucionproducto_ibfk_2` FOREIGN KEY (`DevolucionID`) REFERENCES `devolucion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucionproducto_ibfk_3` FOREIGN KEY (`ProductoDevueltoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `devolucionproducto_ibfk_4` FOREIGN KEY (`ProductoIDID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devolucionproducto_change`
--

DROP TABLE IF EXISTS `devolucionproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucionproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VentaProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DevolucionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoCodigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PorcentajeImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoDevueltoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PrecioDeListaUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoIDID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `devolucionproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `divisa`
--

DROP TABLE IF EXISTS `divisa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `divisa` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Codigo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `divisa_change`
--

DROP TABLE IF EXISTS `divisa_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `divisa_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `divisa_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SendAttempts` int(11) NOT NULL DEFAULT '0',
  `SendLog` longtext,
  `LastSendAttemptDate` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `To` longtext,
  `Cc` longtext,
  `Bcc` longtext,
  `Subject` longtext,
  `Body` longtext,
  `IsBodyHtml` tinyint(1) NOT NULL DEFAULT '1',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`),
  KEY `IX_SendAttempts` (`SendAttempts`),
  KEY `IX_LastSendAttemptDate` (`LastSendAttemptDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailattachment`
--

DROP TABLE IF EXISTS `emailattachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailattachment` (
  `EmailID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Name` varchar(255) DEFAULT '',
  `Content` longblob,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_EmailID` (`EmailID`),
  CONSTRAINT `emailattachment_ibfk_1` FOREIGN KEY (`EmailID`) REFERENCES `email` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailtemplate`
--

DROP TABLE IF EXISTS `emailtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtemplate` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Body` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Subject` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Type` (`Type`),
  KEY `IX_Type` (`Type`),
  KEY `IX_Name` (`Name`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `emailtemplate_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailtemplate_change`
--

DROP TABLE IF EXISTS `emailtemplate_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtemplate_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Subject` longtext COLLATE utf8_unicode_ci,
  `Obj_Body` longtext COLLATE utf8_unicode_ci,
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `emailtemplate_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `empleado_change`
--

DROP TABLE IF EXISTS `empleado_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `empleado_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CP` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Numero` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RFC` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Telefono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DelegacionMunicipio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Calle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroInterior` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RegimenFiscal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Pais` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RegistroPatronal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RazonSocial` varchar(255) DEFAULT '',
  `RegimenPatronal` varchar(255) DEFAULT '',
  `TipoDeDetalleFactura` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoDeRedondeo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DiasParaFacturar` int(11) NOT NULL DEFAULT '15',
  `NumeroDeDecimalesAlRedondearEnFacturas` int(11) NOT NULL DEFAULT '2',
  `CertificadoNombreLlavePublica` longblob,
  `CertificadoNombreLlavePrivada` longblob,
  `CertificadoPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RequerirActivacionDeClientes` tinyint(1) NOT NULL DEFAULT '0',
  `CFDINotificacionEmailAsunto` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `EmailActivacionClienteContenido` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `CFDINotificacionEmailCopia` varchar(255) DEFAULT '',
  `EmailActivacionClienteAsunto` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `CFDINotificacionEmailContenido` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FacturaEmailNotificacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CertificadoNombreLlavePublicaFileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CertificadoNombreLlavePrivadaFileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `EmailActivacionClienteContenidoEsHtml` tinyint(1) NOT NULL DEFAULT '1',
  `FacturacionEnLineaActiva` tinyint(1) NOT NULL DEFAULT '0',
  `FacturasCredito` bigint(20) NOT NULL DEFAULT '0',
  `ModoDeFacturacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FacturasAdquiridas` bigint(20) NOT NULL DEFAULT '0',
  `FacturasEmitidas` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `empresa_change`
--

DROP TABLE IF EXISTS `empresa_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FacturasCredito` bigint(20) NOT NULL DEFAULT '0',
  `Obj_CertificadoNombreLlavePublica` longblob,
  `Obj_CertificadoNombreLlavePublicaFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeDetalleFactura` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegistroPatronal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModoDeFacturacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RequerirActivacionDeClientes` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CFDINotificacionEmailAsunto` longtext COLLATE utf8_unicode_ci,
  `Obj_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificadoNombreLlavePrivada` longblob,
  `Obj_CertificadoNombreLlavePrivadaFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EmailActivacionClienteContenidoEsHtml` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_EmailActivacionClienteContenido` longtext COLLATE utf8_unicode_ci,
  `Obj_FacturacionEnLineaActiva` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FacturaEmailNotificacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeRedondeo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CP` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Colonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DiasParaFacturar` int(11) NOT NULL DEFAULT '0',
  `Obj_Calle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroInterior` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DelegacionMunicipio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Pais` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeDecimalesAlRedondearEnFacturas` int(11) NOT NULL DEFAULT '0',
  `Obj_FacturasAdquiridas` bigint(20) NOT NULL DEFAULT '0',
  `Obj_RFC` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegimenFiscal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EmailActivacionClienteAsunto` longtext COLLATE utf8_unicode_ci,
  `Obj_Numero` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificadoPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CFDINotificacionEmailContenido` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FacturasEmitidas` bigint(20) NOT NULL DEFAULT '0',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `empresa_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `equivalencias`
--

DROP TABLE IF EXISTS `equivalencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equivalencias` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Unidad2ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CantidadEquivalencia` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Unidad1ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_Unidad2ID` (`Unidad2ID`),
  KEY `IX_Unidad1ID` (`Unidad1ID`),
  CONSTRAINT `equivalencias_ibfk_1` FOREIGN KEY (`Unidad2ID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `equivalencias_ibfk_2` FOREIGN KEY (`Unidad1ID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventlog`
--

DROP TABLE IF EXISTS `eventlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventlog` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Message` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Detail` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Type` (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Nombre` (`Nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `evento_change`
--

DROP TABLE IF EXISTS `evento_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `evento_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Folio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RFC` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CFDIUUID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Moneda` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Pesos',
  `SucursalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `NumeroInterior` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Pais` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Municipio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Calle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CodigoPostal` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Serie` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroExterior` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `RegistradaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ModoDeFacturacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SalarioBase` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CanceladaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CancelacionAutorizadaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ArchivoPDF` longblob,
  `ArchivoXML` longblob,
  `NumeroDiasPagados` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MotivoCancelacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TipoDeDetalle` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Detallada',
  `Estatus` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Log` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `UrlPDF` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SalarioDiarioIntegrado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Tipo` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'CFDI',
  `FechaInicialPago` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TipoComprobante` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'ingreso',
  `CancelacionNotificada` tinyint(1) NOT NULL DEFAULT '0',
  `TurnoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `EmpleadoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FechaPago` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `UrlXML` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `UrlAcuseDeCancelacionXML` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Referencia` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `VentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_SucursalID` (`SucursalID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_TurnoID` (`TurnoID`),
  KEY `IX_VentaID` (`VentaID`),
  KEY `IX_RegistradaPorID` (`RegistradaPorID`),
  KEY `IX_CanceladaPorID` (`CanceladaPorID`),
  KEY `IX_CancelacionAutorizadaPorID` (`CancelacionAutorizadaPorID`),
  KEY `IX_EmpleadoID` (`EmpleadoID`),
  CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`SucursalID`) REFERENCES `sucursal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_10` FOREIGN KEY (`CanceladaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_11` FOREIGN KEY (`CancelacionAutorizadaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_12` FOREIGN KEY (`EmpleadoID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_5` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_6` FOREIGN KEY (`TurnoID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_8` FOREIGN KEY (`VentaID`) REFERENCES `venta` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `factura_ibfk_9` FOREIGN KEY (`RegistradaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `factura_change`
--

DROP TABLE IF EXISTS `factura_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegistradaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModoDeFacturacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SalarioBase` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RFC` varchar(13) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_CanceladaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CFDIUUID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Moneda` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CancelacionAutorizadaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SucursalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ArchivoPDF` longblob,
  `Obj_ArchivoXML` longblob,
  `Obj_NumeroDiasPagados` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroInterior` varchar(25) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Colonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Pais` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Municipio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MotivoCancelacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estado` varchar(120) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeDetalle` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estatus` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia` longtext COLLATE utf8_unicode_ci,
  `Obj_Calle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Log` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UrlPDF` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CodigoPostal` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SalarioDiarioIntegrado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Ciudad` varchar(120) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaInicialPago` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TipoComprobante` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Serie` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CancelacionNotificada` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_TurnoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EmpleadoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroExterior` varchar(25) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaPago` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_UrlXML` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_UrlAcuseDeCancelacionXML` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_VentaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Email` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `factura_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `failedauthenticationattempt`
--

DROP TABLE IF EXISTS `failedauthenticationattempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failedauthenticationattempt` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `IP` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Username` (`Username`),
  KEY `IX_IP` (`IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formadepago`
--

DROP TABLE IF EXISTS `formadepago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formadepago` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CambioMaximo` decimal(26,10) NOT NULL DEFAULT '900.0000000000',
  `Activo` tinyint(1) NOT NULL DEFAULT '0',
  `AbrirCajonDeDinero` tinyint(1) NOT NULL DEFAULT '0',
  `TipoDeCambio` decimal(26,10) NOT NULL DEFAULT '1.0000000000',
  `MontoDefault` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '01',
  `PermitirDarCambio` tinyint(1) NOT NULL DEFAULT '1',
  `Orden` int(11) NOT NULL DEFAULT '1',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PermitirModificarCantidad` tinyint(1) NOT NULL DEFAULT '1',
  `TipoReferencia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'NoRequerir',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formadepago_change`
--

DROP TABLE IF EXISTS `formadepago_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formadepago_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CambioMaximo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Activo` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AbrirCajonDeDinero` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_TipoReferencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeCambio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MontoDefault` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PermitirDarCambio` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Orden` int(11) NOT NULL DEFAULT '0',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PermitirModificarCantidad` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `formadepago_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formadepagodenominacion`
--

DROP TABLE IF EXISTS `formadepagodenominacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formadepagodenominacion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Imagen2` longblob,
  `Imagen2FileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Valor` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Imagen` longblob,
  `ImagenFileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  CONSTRAINT `formadepagodenominacion_ibfk_1` FOREIGN KEY (`FormaDePagoID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formadepagodenominacion_change`
--

DROP TABLE IF EXISTS `formadepagodenominacion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formadepagodenominacion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Imagen` longblob,
  `Obj_ImagenFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Valor` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FormaDePagoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `formadepagodenominacion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formatodeimpresion`
--

DROP TABLE IF EXISTS `formatodeimpresion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formatodeimpresion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Altura` double NOT NULL DEFAULT '0',
  `Activo` tinyint(1) NOT NULL DEFAULT '0',
  `Ancho` double NOT NULL DEFAULT '0',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Datos` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Version` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formatodeimpresion_change`
--

DROP TABLE IF EXISTS `formatodeimpresion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formatodeimpresion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Altura` double NOT NULL DEFAULT '0',
  `Obj_Datos` longtext COLLATE utf8_unicode_ci,
  `Obj_Activo` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Version` int(11) NOT NULL DEFAULT '0',
  `Obj_Ancho` double NOT NULL DEFAULT '0',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `formatodeimpresion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formatodeimpresiondefault`
--

DROP TABLE IF EXISTS `formatodeimpresiondefault`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formatodeimpresiondefault` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Altura` double NOT NULL DEFAULT '0',
  `Ancho` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Ticket',
  `Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Datos` longtext COLLATE utf8_unicode_ci,
  `Version` int(11) NOT NULL DEFAULT '1',
  `Activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formatodeimpresiondefault_change`
--

DROP TABLE IF EXISTS `formatodeimpresiondefault_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formatodeimpresiondefault_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Altura` double NOT NULL DEFAULT '0',
  `Obj_Ancho` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Datos` longtext COLLATE utf8_unicode_ci,
  `Obj_Version` int(11) NOT NULL DEFAULT '0',
  `Obj_Activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `formatodeimpresiondefault_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `girodenegocio`
--

DROP TABLE IF EXISTS `girodenegocio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `girodenegocio` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `EmpresaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PaginaWeb` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Notas` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LogoNombreDeArchivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Logo2` longblob,
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Logo2FileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_EmpresaID` (`EmpresaID`),
  CONSTRAINT `girodenegocio_ibfk_1` FOREIGN KEY (`EmpresaID`) REFERENCES `empresa` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `girodenegocio_change`
--

DROP TABLE IF EXISTS `girodenegocio_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `girodenegocio_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PaginaWeb` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Notas` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LogoNombreDeArchivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Logo2` longblob,
  `Obj_Logo2FileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `girodenegocio_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `globalsequence`
--

DROP TABLE IF EXISTS `globalsequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsequence` (
  `Sequence` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Sequence` (`Sequence`),
  KEY `IX_Sequence` (`Sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grupomodificador`
--

DROP TABLE IF EXISTS `grupomodificador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupomodificador` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `MaximoASeleccionar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoDeSeleccion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Individual',
  `Color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#8b008b',
  `MinimoASeleccionar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `RegresoDescansoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `CantidadASeleccionar` int(11) NOT NULL DEFAULT '0',
  `CantidadASeleccionarTo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grupomodificador_change`
--

DROP TABLE IF EXISTS `grupomodificador_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupomodificador_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeSeleccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CantidadASeleccionar` int(11) NOT NULL DEFAULT '0',
  `Obj_CantidadASeleccionarTo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `grupomodificador_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grupomodificadormodificador`
--

DROP TABLE IF EXISTS `grupomodificadormodificador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupomodificadormodificador` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ModificadorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Preseleccionado` tinyint(1) NOT NULL DEFAULT '0',
  `GrupoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Excluyente` tinyint(1) NOT NULL DEFAULT '0',
  `MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#8b008b',
  `Cobrar` tinyint(1) NOT NULL DEFAULT '0',
  `CantidadMaxima` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_ModificadorID` (`ModificadorID`),
  KEY `IX_GrupoID` (`GrupoID`),
  CONSTRAINT `grupomodificadormodificador_ibfk_1` FOREIGN KEY (`ModificadorID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `grupomodificadormodificador_ibfk_2` FOREIGN KEY (`GrupoID`) REFERENCES `grupomodificador` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grupomodificadormodificador_change`
--

DROP TABLE IF EXISTS `grupomodificadormodificador_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupomodificadormodificador_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModificadorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Preseleccionado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_GrupoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Excluyente` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cobrar` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CantidadMaxima` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `grupomodificadormodificador_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `TurnoJueves` varchar(255) DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TurnoDomingo` varchar(255) DEFAULT '',
  `ToleranciaRetardo` int(11) NOT NULL DEFAULT '0',
  `TurnoMiercoles` varchar(255) DEFAULT '',
  `TurnoLunes` varchar(255) DEFAULT '',
  `TurnoMartes` varchar(255) DEFAULT '',
  `TurnoSabado` varchar(255) DEFAULT '',
  `TurnoViernes` varchar(255) DEFAULT '',
  `ActivoJueves` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `ActivoDomingo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `ActivoMiercoles` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `ActivoLunes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `ActivoMartes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `ActivoSabado` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `ActivoViernes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SeTrabaja',
  `SalidaComidaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 10:00:00.000000',
  `TiempoDeTrabajoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `TiempoDeComidaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `TurnoLunesID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegresoDescansoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeDescansoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `TiempoDeComidaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `RegresoComidaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoDescansoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TipoDeHorarioTrabajo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Fijo',
  `SalidaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeDescansoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `TiempoDeTrabajoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `RegresoComidaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeComidaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `SalidaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeComidaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `SalidaComidaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeTrabajoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `SalidaDescansoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 09:00:00.000000',
  `TiempoDeTrabajoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `EntradaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoDescansoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TurnoMartesID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TiempoDeComidaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `EntradaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoComidaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoDescansoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TipoDeHorarioDescanso` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SinHorario',
  `EntradaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoDescansoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeTrabajoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `SalidaDescansoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeDescansoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `TiempoDeComidaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `SalidaComidaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoDescansoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoComidaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoComidaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeTrabajoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `TurnoViernesID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TiempoDeDescansoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `TiempoDeComidaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:00:00.000000',
  `SalidaComidaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TurnoDomingoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TiempoDeDescansoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `SalidaDescansoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TipoDeHorarioComida` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'SinHorario',
  `SalidaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeTrabajoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 08:00:00.000000',
  `RegresoComidaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TiempoDeDescansoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `TiempoDeDescansoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 00:30:00.000000',
  `TurnoMiercolesID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TurnoSabadoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `SalidaDescansoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RegresoComidaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TurnoJuevesID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegresoDescansoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaLunesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaMartesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 09:00:00.000000',
  `EntradaMiercolesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaSabadoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaDomingoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaViernesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `EntradaJuevesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaSabadoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 10:00:00.000000',
  `SalidaComidaLunesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaJuevesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaViernesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaMartesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaMiercolesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaComidaDomingoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoSabadoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoViernesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoLunesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoDomingoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoMartesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoJuevesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `SalidaDescansoMiercolesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  PRIMARY KEY (`ID`),
  KEY `IX_TurnoLunesID` (`TurnoLunesID`),
  KEY `IX_TurnoMartesID` (`TurnoMartesID`),
  KEY `IX_TurnoViernesID` (`TurnoViernesID`),
  KEY `IX_TurnoDomingoID` (`TurnoDomingoID`),
  KEY `IX_TurnoMiercolesID` (`TurnoMiercolesID`),
  KEY `IX_TurnoSabadoID` (`TurnoSabadoID`),
  KEY `IX_TurnoJuevesID` (`TurnoJuevesID`),
  CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`TurnoLunesID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `horario_ibfk_2` FOREIGN KEY (`TurnoMartesID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `horario_ibfk_3` FOREIGN KEY (`TurnoViernesID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `horario_ibfk_4` FOREIGN KEY (`TurnoDomingoID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `horario_ibfk_5` FOREIGN KEY (`TurnoMiercolesID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `horario_ibfk_6` FOREIGN KEY (`TurnoSabadoID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `horario_ibfk_7` FOREIGN KEY (`TurnoJuevesID`) REFERENCES `turno` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horario_change`
--

DROP TABLE IF EXISTS `horario_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ActivoJueves` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SalidaComidaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaSabadoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeTrabajoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeComidaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TurnoLunesID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeDescansoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeComidaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TipoDeHorarioTrabajo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ActivoDomingo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ToleranciaRetardo` int(11) NOT NULL DEFAULT '0',
  `Obj_TiempoDeDescansoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeTrabajoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaLunesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaJuevesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoSabadoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeComidaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeComidaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaViernesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeTrabajoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoViernesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaMartesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaMartesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeTrabajoSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaMiercolesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TurnoMartesID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeComidaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaSabado` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaSabadoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoLunesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TipoDeHorarioDescanso` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EntradaLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaLunesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeTrabajoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoDomingoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeDescansoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ActivoMiercoles` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeComidaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaMiercolesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ActivoLunes` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EntradaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaDomingoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaViernesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ActivoMartes` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeTrabajoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TurnoViernesID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeDescansoDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeComidaMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaDomingo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaComidaDomingoTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TurnoDomingoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeDescansoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoMartes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoMartesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TipoDeHorarioComida` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EntradaJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_EntradaJuevesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeTrabajoLunes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ActivoSabado` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TiempoDeDescansoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TiempoDeDescansoViernes` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ActivoViernes` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TurnoMiercolesID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TurnoSabadoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SalidaDescansoJueves` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoJuevesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoMiercoles` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalidaDescansoMiercolesTo` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TurnoJuevesID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `horario_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installedmodule`
--

DROP TABLE IF EXISTS `installedmodule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installedmodule` (
  `Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Summary` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext COLLATE utf8_unicode_ci,
  `Image` longtext COLLATE utf8_unicode_ci,
  `InstalledVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `MinimumPlatformVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `Package` longtext COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_Name` (`Name`),
  KEY `IX_InstalledVersion` (`InstalledVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installedmodule_change`
--

DROP TABLE IF EXISTS `installedmodule_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installedmodule_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Summary` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Image` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MinimumPlatformVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Package` longtext COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `installedmodule_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installedmoduledependency`
--

DROP TABLE IF EXISTS `installedmoduledependency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installedmoduledependency` (
  `ModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DependsOnModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DependsOnModuleVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_ModuleID` (`ModuleID`),
  KEY `IX_DependsOnModuleID` (`DependsOnModuleID`),
  CONSTRAINT `installedmoduledependency_ibfk_1` FOREIGN KEY (`ModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `installedmoduledependency_ibfk_2` FOREIGN KEY (`DependsOnModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installedmoduledependency_change`
--

DROP TABLE IF EXISTS `installedmoduledependency_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installedmoduledependency_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DependsOnModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DependsOnModuleVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `installedmoduledependency_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installmodulerequest`
--

DROP TABLE IF EXISTS `installmodulerequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installmodulerequest` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RequestedByID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Status` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `StatusDetails` longtext COLLATE utf8_unicode_ci,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Install',
  `ModuleIdentifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ModuleName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ModuleSummary` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ModuleDescription` longtext COLLATE utf8_unicode_ci,
  `ModuleImage` longtext COLLATE utf8_unicode_ci,
  `Prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `MinimumPlatformVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ModulePackage` longtext COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`),
  KEY `IX_RequestedByID` (`RequestedByID`),
  KEY `IX_Status` (`Status`),
  KEY `IX_Action` (`Action`),
  KEY `IX_ModuleIdentifier` (`ModuleIdentifier`),
  KEY `IX_ModuleName` (`ModuleName`),
  CONSTRAINT `installmodulerequest_ibfk_1` FOREIGN KEY (`RequestedByID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installmodulerequest_change`
--

DROP TABLE IF EXISTS `installmodulerequest_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installmodulerequest_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_RequestedByID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Status` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_StatusDetails` longtext COLLATE utf8_unicode_ci,
  `Obj_Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModuleIdentifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModuleName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModuleSummary` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModuleDescription` longtext COLLATE utf8_unicode_ci,
  `Obj_ModuleImage` longtext COLLATE utf8_unicode_ci,
  `Obj_Prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_MinimumPlatformVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModulePackage` longtext COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `installmodulerequest_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menuitem`
--

DROP TABLE IF EXISTS `menuitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menuitem` (
  `MenuItemID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `IconClass` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `UIComponentID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `UIComponentData` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `URL` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `URLTarget` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '_blank',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `Order` bigint(20) NOT NULL DEFAULT '1000',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RequiredPermissions` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_MenuItemID` (`MenuItemID`),
  KEY `IX_UIComponentID` (`UIComponentID`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `menuitem_ibfk_1` FOREIGN KEY (`MenuItemID`) REFERENCES `menuitem` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `menuitem_ibfk_2` FOREIGN KEY (`UIComponentID`) REFERENCES `uicomponent` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `menuitem_ibfk_3` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menuitem_change`
--

DROP TABLE IF EXISTS `menuitem_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menuitem_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MenuItemID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RequiredPermissions` longtext COLLATE utf8_unicode_ci,
  `Obj_IconClass` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UIComponentID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UIComponentData` longtext COLLATE utf8_unicode_ci,
  `Obj_URL` longtext COLLATE utf8_unicode_ci,
  `Obj_URLTarget` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Order` bigint(20) NOT NULL DEFAULT '0',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `menuitem_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `modulecatalog`
--

DROP TABLE IF EXISTS `modulecatalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulecatalog` (
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Username` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `modulecatalog_change`
--

DROP TABLE IF EXISTS `modulecatalog_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulecatalog_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Username` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `modulecatalog_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduledefinition`
--

DROP TABLE IF EXISTS `moduledefinition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduledefinition` (
  `Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `MinimumPlatformVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Data` longtext COLLATE utf8_unicode_ci,
  `Dependencies` longtext COLLATE utf8_unicode_ci,
  `BeforeInstall` longtext COLLATE utf8_unicode_ci,
  `BeforeUpdate` longtext COLLATE utf8_unicode_ci,
  `BeforeRepair` longtext COLLATE utf8_unicode_ci,
  `BeforeUninstall` longtext COLLATE utf8_unicode_ci,
  `AfterInstall` longtext COLLATE utf8_unicode_ci,
  `AfterUpdate` longtext COLLATE utf8_unicode_ci,
  `AfterRepair` longtext COLLATE utf8_unicode_ci,
  `AfterUninstall` longtext COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `moduledefinition_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduledefinition_change`
--

DROP TABLE IF EXISTS `moduledefinition_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduledefinition_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MinimumPlatformVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Data` longtext COLLATE utf8_unicode_ci,
  `Obj_Dependencies` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeInstall` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUpdate` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeRepair` longtext COLLATE utf8_unicode_ci,
  `Obj_BeforeUninstall` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterInstall` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterUpdate` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterRepair` longtext COLLATE utf8_unicode_ci,
  `Obj_AfterUninstall` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `moduledefinition_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motivodecancelacion`
--

DROP TABLE IF EXISTS `motivodecancelacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motivodecancelacion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `motivodecancelacion_change`
--

DROP TABLE IF EXISTS `motivodecancelacion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motivodecancelacion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `motivodecancelacion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventario`
--

DROP TABLE IF EXISTS `movimientoinventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventario` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProveedorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Notas` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `CuentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FechaAplicacion` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `RegistradoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `AlmacenID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `NumeroDeTerminal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FolioUnico` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SeparadorFolioDeVenta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '-',
  `Folio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FolioSecuencial` bigint(20) NOT NULL DEFAULT '0',
  `FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `CanceladoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `CancelacionAutorizadaPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `MotivoCancelacion` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Cancelado` tinyint(1) NOT NULL DEFAULT '0',
  `TurnoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DevolucionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CostoCalculado` tinyint(1) NOT NULL DEFAULT '0',
  `CanceladoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CancelacionAutorizadaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegistradoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `VentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_ProveedorID` (`ProveedorID`),
  KEY `IX_CuentaID` (`CuentaID`),
  KEY `IX_AlmacenID` (`AlmacenID`),
  KEY `IX_TurnoID` (`TurnoID`),
  KEY `IX_DevolucionID` (`DevolucionID`),
  KEY `IX_CanceladoPorID` (`CanceladoPorID`),
  KEY `IX_CancelacionAutorizadaPorID` (`CancelacionAutorizadaPorID`),
  KEY `IX_RegistradoPorID` (`RegistradoPorID`),
  KEY `IX_VentaID` (`VentaID`),
  CONSTRAINT `movimientoinventario_ibfk_1` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_10` FOREIGN KEY (`VentaID`) REFERENCES `venta` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_2` FOREIGN KEY (`ProveedorID`) REFERENCES `proveedores` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_3` FOREIGN KEY (`CuentaID`) REFERENCES `cuentabancaria` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_4` FOREIGN KEY (`AlmacenID`) REFERENCES `almacen` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_5` FOREIGN KEY (`TurnoID`) REFERENCES `turnoterminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_6` FOREIGN KEY (`DevolucionID`) REFERENCES `devolucion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_7` FOREIGN KEY (`CanceladoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_8` FOREIGN KEY (`CancelacionAutorizadaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventario_ibfk_9` FOREIGN KEY (`RegistradoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventario_change`
--

DROP TABLE IF EXISTS `movimientoinventario_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventario_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CanceladoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaAplicacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Referencia` longtext COLLATE utf8_unicode_ci,
  `Obj_Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_CancelacionAutorizadaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegistradoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MotivoCancelacion` longtext COLLATE utf8_unicode_ci,
  `Obj_ProveedorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AlmacenID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DevolucionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cancelado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FolioUnico` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoCalculado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_SeparadorFolioDeVenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Notas` longtext COLLATE utf8_unicode_ci,
  `Obj_TurnoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FolioSecuencial` bigint(20) NOT NULL DEFAULT '0',
  `Obj_FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_CuentaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_VentaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `movimientoinventario_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventariopeps`
--

DROP TABLE IF EXISTS `movimientoinventariopeps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventariopeps` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProductoEntradaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoSalidaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoEntradaID` (`ProductoEntradaID`),
  KEY `IX_ProductoSalidaID` (`ProductoSalidaID`),
  CONSTRAINT `movimientoinventariopeps_ibfk_1` FOREIGN KEY (`ProductoEntradaID`) REFERENCES `movimientoinventarioproducto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventariopeps_ibfk_2` FOREIGN KEY (`ProductoSalidaID`) REFERENCES `movimientoinventarioproducto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventariopeps_change`
--

DROP TABLE IF EXISTS `movimientoinventariopeps_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventariopeps_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoEntradaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoSalidaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `movimientoinventariopeps_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventarioproducto`
--

DROP TABLE IF EXISTS `movimientoinventarioproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventarioproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `MovimientoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `VentaProducto` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Cancelado` tinyint(1) NOT NULL DEFAULT '0',
  `CostoTotalSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CantidadUEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FechaAplicacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Producto` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Almacen` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DevolucionProducto` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CantidadPEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto3UEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto2UEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto1UEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto3PEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto2Promedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalSinImpuestoPEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalSinImpuestoPromedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoCalculado` tinyint(1) NOT NULL DEFAULT '0',
  `CostoTotalConImpuestoPromedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto2PEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalConImpuestoPEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto1PEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalConImpuestoUEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalSinImpuestoUEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto3Promedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalImpuesto1Promedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoTotalConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TipoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `AlmacenID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DevolucionProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `VentaProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TipoMovimientoInventario` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_MovimientoID` (`MovimientoID`),
  KEY `IX_TipoID` (`TipoID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_AlmacenID` (`AlmacenID`),
  KEY `IX_DevolucionProductoID` (`DevolucionProductoID`),
  KEY `IX_VentaProductoID` (`VentaProductoID`),
  CONSTRAINT `movimientoinventarioproducto_ibfk_1` FOREIGN KEY (`MovimientoID`) REFERENCES `movimientoinventario` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventarioproducto_ibfk_2` FOREIGN KEY (`TipoID`) REFERENCES `tipomovimientoinventario` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventarioproducto_ibfk_3` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventarioproducto_ibfk_4` FOREIGN KEY (`AlmacenID`) REFERENCES `almacen` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventarioproducto_ibfk_5` FOREIGN KEY (`DevolucionProductoID`) REFERENCES `devolucionproducto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventarioproducto_ibfk_6` FOREIGN KEY (`VentaProductoID`) REFERENCES `ventaproducto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventarioproducto_change`
--

DROP TABLE IF EXISTS `movimientoinventarioproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventarioproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CostoTotalImpuesto3UEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Cancelado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CostoTotalSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto2UEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto1UEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto3PEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TipoMovimientoInventario` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CantidadUEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto2Promedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FechaAplicacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AlmacenID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalSinImpuestoPEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DevolucionProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CantidadPEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_VentaProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CostoTotalSinImpuestoPromedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoCalculado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CostoTotalConImpuestoPromedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto2PEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalConImpuestoPEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto1PEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalConImpuestoUEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalSinImpuestoUEPS` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CostoTotalImpuesto3Promedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MovimientoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CostoTotalImpuesto1Promedio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `movimientoinventarioproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventarioueps`
--

DROP TABLE IF EXISTS `movimientoinventarioueps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventarioueps` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoSalidaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProductoEntradaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoSalidaID` (`ProductoSalidaID`),
  KEY `IX_ProductoEntradaID` (`ProductoEntradaID`),
  CONSTRAINT `movimientoinventarioueps_ibfk_1` FOREIGN KEY (`ProductoSalidaID`) REFERENCES `movimientoinventarioproducto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `movimientoinventarioueps_ibfk_2` FOREIGN KEY (`ProductoEntradaID`) REFERENCES `movimientoinventarioproducto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimientoinventarioueps_change`
--

DROP TABLE IF EXISTS `movimientoinventarioueps_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoinventarioueps_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoSalidaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoEntradaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `movimientoinventarioueps_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `municipio`
--

DROP TABLE IF EXISTS `municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `municipio` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) DEFAULT '',
  `Fundacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  PRIMARY KEY (`ID`),
  KEY `IX_Fundacion` (`Fundacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `netassembly`
--

DROP TABLE IF EXISTS `netassembly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `netassembly` (
  `Assembly` longblob,
  `AssemblyFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext COLLATE utf8_unicode_ci,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `AssemblyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `AssemblyVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `AssemblyPublicKeyToken` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `AssemblyCulture` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  KEY `IX_AssemblyName` (`AssemblyName`),
  KEY `IX_AssemblyVersion` (`AssemblyVersion`),
  KEY `IX_AssemblyPublicKeyToken` (`AssemblyPublicKeyToken`),
  KEY `IX_AssemblyCulture` (`AssemblyCulture`),
  CONSTRAINT `netassembly_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `netassembly_change`
--

DROP TABLE IF EXISTS `netassembly_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `netassembly_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Assembly` longblob,
  `Obj_AssemblyFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AssemblyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AssemblyVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AssemblyPublicKeyToken` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AssemblyCulture` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `netassembly_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `passwordresetrequest`
--

DROP TABLE IF EXISTS `passwordresetrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordresetrequest` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `AuthorizationCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Retries` smallint(6) NOT NULL DEFAULT '0',
  `UserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`),
  KEY `IX_AuthorizationCode` (`AuthorizationCode`),
  KEY `IX_UserID` (`UserID`),
  KEY `IX_Email` (`Email`),
  CONSTRAINT `passwordresetrequest_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Estatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `InformacionAdicional` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `FechaProcesamiento` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProcesadoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DireccionDeEnvioID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `SeparadorFolioDeVenta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDeTerminal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RegistradoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaDeRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `FolioUnico` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoSubtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FolioSecuencial` bigint(20) NOT NULL DEFAULT '0',
  `Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoPorcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MotivoCancelacion` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto2PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CanceladoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ComentariosProcesamiento` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Folio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaDeEntrega` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `Impuesto3PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Vendedor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `VendedorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProcesadoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegistradoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CanceladoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_ClienteID` (`ClienteID`),
  KEY `IX_DireccionDeEnvioID` (`DireccionDeEnvioID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_FolioSecuencial` (`FolioSecuencial`),
  KEY `IX_VendedorID` (`VendedorID`),
  KEY `IX_ProcesadoPorID` (`ProcesadoPorID`),
  KEY `IX_RegistradoPorID` (`RegistradoPorID`),
  KEY `IX_CanceladoPorID` (`CanceladoPorID`),
  CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`DireccionDeEnvioID`) REFERENCES `clientedireccion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_4` FOREIGN KEY (`VendedorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_5` FOREIGN KEY (`ProcesadoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_6` FOREIGN KEY (`RegistradoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_7` FOREIGN KEY (`CanceladoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pedido_change`
--

DROP TABLE IF EXISTS `pedido_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estatus` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_InformacionAdicional` longtext COLLATE utf8_unicode_ci,
  `Obj_SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_FechaProcesamiento` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto1PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProcesadoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DireccionDeEnvioID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SeparadorFolioDeVenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegistradoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaDeRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_FolioUnico` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoSubtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FolioSecuencial` bigint(20) NOT NULL DEFAULT '0',
  `Obj_Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoPorcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MotivoCancelacion` longtext COLLATE utf8_unicode_ci,
  `Obj_DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto2PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CanceladoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ComentariosProcesamiento` longtext COLLATE utf8_unicode_ci,
  `Obj_Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaDeEntrega` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Impuesto3PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Vendedor` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VendedorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `pedido_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pedidoproducto`
--

DROP TABLE IF EXISTS `pedidoproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidoproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PrecioDeListaUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVentaUnitario2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVentaUnitario3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeVentaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeVentaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CatalogoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProductoCodigo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PorcentajeImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoAutorizadoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DescuentoUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoReferencia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PrecioDeListaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PedidoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProductoNombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ProductoCantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVentaUnitario1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CantidadFija` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoAutorizadoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CatalogoID` (`CatalogoID`),
  KEY `IX_DescuentoID` (`DescuentoID`),
  KEY `IX_PedidoID` (`PedidoID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_DescuentoAutorizadoPorID` (`DescuentoAutorizadoPorID`),
  CONSTRAINT `pedidoproducto_ibfk_1` FOREIGN KEY (`CatalogoID`) REFERENCES `catalogodeproductos` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedidoproducto_ibfk_2` FOREIGN KEY (`DescuentoID`) REFERENCES `descuento` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedidoproducto_ibfk_3` FOREIGN KEY (`PedidoID`) REFERENCES `pedido` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedidoproducto_ibfk_4` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `pedidoproducto_ibfk_5` FOREIGN KEY (`DescuentoAutorizadoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pedidoproducto_change`
--

DROP TABLE IF EXISTS `pedidoproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidoproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PrecioDeListaUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVentaUnitario2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CantidadFija` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVentaUnitario3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeVentaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeVentaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CatalogoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoCodigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PorcentajeImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoAutorizadoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoReferencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PrecioDeListaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PedidoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoCantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVentaUnitario1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `pedidoproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pendingaction`
--

DROP TABLE IF EXISTS `pendingaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingaction` (
  `Action` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Arguments` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_Action` (`Action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Edad` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_Nombre` (`Nombre`),
  KEY `IX_Edad` (`Edad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_change`
--

DROP TABLE IF EXISTS `persona_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Edad` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `persona_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Codigo` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Descripcion` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clasificacion1ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `UnidadID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TipoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clasificacion3` varchar(255) DEFAULT '',
  `Clasificacion2ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Clasificacion3ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CategoriaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PrecioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeMerma` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MaximoModificadores` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CobrarModificadoresEnProducto` tinyint(1) NOT NULL DEFAULT '0',
  `AplicarImpuesto3AlComprar` tinyint(1) NOT NULL DEFAULT '0',
  `RedondearEnCompras` tinyint(1) NOT NULL DEFAULT '1',
  `AplicarImpuesto1AlVender` tinyint(1) NOT NULL DEFAULT '0',
  `MultiplicarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `AplicarImpuesto3AlVender` tinyint(1) NOT NULL DEFAULT '0',
  `InventarioOptimoDefault` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeUtilidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MinimoModificadores` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PuntoDeReordenDefault` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CostoUnitario` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TipoDeVenta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Pieza',
  `Color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#8b008b',
  `AplicarImpuesto2AlVender` tinyint(1) NOT NULL DEFAULT '0',
  `AplicarImpuesto2AlComprar` tinyint(1) NOT NULL DEFAULT '0',
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  `MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `AplicarImpuesto1AlComprar` tinyint(1) NOT NULL DEFAULT '0',
  `DescuentoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CodigoDeBarras` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Subtipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Producto',
  `Imagen` longblob,
  `ImagenFileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ComisionComisionista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ComisionMesero` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestosAlComprar` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ImpuestosAlVender` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Codigo` (`Codigo`),
  KEY `IX_Codigo` (`Codigo`),
  KEY `IX_Nombre` (`Nombre`),
  KEY `IX_Clasificacion1ID` (`Clasificacion1ID`),
  KEY `IX_UnidadID` (`UnidadID`),
  KEY `IX_TipoID` (`TipoID`),
  KEY `IX_Clasificacion2ID` (`Clasificacion2ID`),
  KEY `IX_Clasificacion3ID` (`Clasificacion3ID`),
  KEY `IX_CategoriaID` (`CategoriaID`),
  KEY `IX_DescuentoID` (`DescuentoID`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`Clasificacion1ID`) REFERENCES `clasificacion1` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`UnidadID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `producto_ibfk_3` FOREIGN KEY (`TipoID`) REFERENCES `tipodeproducto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `producto_ibfk_4` FOREIGN KEY (`Clasificacion2ID`) REFERENCES `clasificacion2` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `producto_ibfk_5` FOREIGN KEY (`Clasificacion3ID`) REFERENCES `clasificacion3` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `producto_ibfk_6` FOREIGN KEY (`CategoriaID`) REFERENCES `categoriadeproducto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `producto_ibfk_7` FOREIGN KEY (`DescuentoID`) REFERENCES `descuento` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `producto_change`
--

DROP TABLE IF EXISTS `producto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CategoriaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PrecioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeMerma` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CodigoDeBarras` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MaximoModificadores` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Subtipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CobrarModificadoresEnProducto` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Referencia3` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AplicarImpuesto3AlComprar` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_RedondearEnCompras` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AplicarImpuesto1AlVender` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Referencia1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ImpuestosAlComprar` longtext COLLATE utf8_unicode_ci,
  `Obj_Clasificacion2ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MultiplicarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AplicarImpuesto3AlVender` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Imagen` longblob,
  `Obj_ImagenFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InventarioOptimoDefault` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeUtilidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ComisionComisionista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Clasificacion3ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MinimoModificadores` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Codigo` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PuntoDeReordenDefault` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestosAlVender` longtext COLLATE utf8_unicode_ci,
  `Obj_Clasificacion1ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CostoUnitario` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TipoDeVenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AplicarImpuesto2AlVender` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AplicarImpuesto2AlComprar` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_UnidadID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Activo` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_ComisionMesero` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TipoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AplicarImpuesto1AlComprar` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Descripcion` longtext COLLATE utf8_unicode_ci,
  `Obj_Nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `producto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productoconversion`
--

DROP TABLE IF EXISTS `productoconversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productoconversion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CantidadFija` tinyint(1) NOT NULL DEFAULT '0',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PorcentajeCosto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ConversionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `UnidadID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_ConversionID` (`ConversionID`),
  KEY `IX_UnidadID` (`UnidadID`),
  CONSTRAINT `productoconversion_ibfk_1` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `productoconversion_ibfk_2` FOREIGN KEY (`ConversionID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `productoconversion_ibfk_3` FOREIGN KEY (`UnidadID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productoconversion_change`
--

DROP TABLE IF EXISTS `productoconversion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productoconversion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CantidadFija` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UnidadID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PorcentajeCosto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ConversionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `productoconversion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productoingrediente`
--

DROP TABLE IF EXISTS `productoingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productoingrediente` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `SoloParaLlevar` tinyint(1) NOT NULL DEFAULT '0',
  `IngredienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `SoloADomicilio` tinyint(1) NOT NULL DEFAULT '0',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `UnidadID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_IngredienteID` (`IngredienteID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_UnidadID` (`UnidadID`),
  CONSTRAINT `productoingrediente_ibfk_1` FOREIGN KEY (`IngredienteID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `productoingrediente_ibfk_2` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `productoingrediente_ibfk_3` FOREIGN KEY (`UnidadID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productoingrediente_change`
--

DROP TABLE IF EXISTS `productoingrediente_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productoingrediente_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SoloParaLlevar` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_IngredienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_SoloADomicilio` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UnidadID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `productoingrediente_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productomodificador`
--

DROP TABLE IF EXISTS `productomodificador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productomodificador` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ModificadorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Preseleccionado` tinyint(1) NOT NULL DEFAULT '0',
  `CantidadMaxima` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#8b008b',
  `MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Cobrar` tinyint(1) NOT NULL DEFAULT '0',
  `Excluyente` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_ModificadorID` (`ModificadorID`),
  CONSTRAINT `productomodificador_ibfk_1` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `productomodificador_ibfk_2` FOREIGN KEY (`ModificadorID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productomodificador_change`
--

DROP TABLE IF EXISTS `productomodificador_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productomodificador_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ModificadorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Preseleccionado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CantidadMaxima` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Color` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Cobrar` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Excluyente` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `productomodificador_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productomodificadorgrupo`
--

DROP TABLE IF EXISTS `productomodificadorgrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productomodificadorgrupo` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `GrupoModificadorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_GrupoModificadorID` (`GrupoModificadorID`),
  CONSTRAINT `productomodificadorgrupo_ibfk_2` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `productomodificadorgrupo_ibfk_3` FOREIGN KEY (`GrupoModificadorID`) REFERENCES `grupomodificador` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productomodificadorgrupo_change`
--

DROP TABLE IF EXISTS `productomodificadorgrupo_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productomodificadorgrupo_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_GrupoModificadorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `productomodificadorgrupo_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programadelealtad`
--

DROP TABLE IF EXISTS `programadelealtad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programadelealtad` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CantidadDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programadelealtad_change`
--

DROP TABLE IF EXISTS `programadelealtad_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programadelealtad_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CantidadDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `programadelealtad_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programadelealtadproducto`
--

DROP TABLE IF EXISTS `programadelealtadproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programadelealtadproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PorcentajeDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CantidadDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProgramaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_ProgramaID` (`ProgramaID`),
  CONSTRAINT `programadelealtadproducto_ibfk_1` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `programadelealtadproducto_ibfk_2` FOREIGN KEY (`ProgramaID`) REFERENCES `programadelealtad` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programadelealtadproducto_change`
--

DROP TABLE IF EXISTS `programadelealtadproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programadelealtadproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PorcentajeDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CantidadDeAcumulacion` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProgramaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `programadelealtadproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programadelealtadrecompensa`
--

DROP TABLE IF EXISTS `programadelealtadrecompensa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programadelealtadrecompensa` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PuntosRequeridos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Activa` tinyint(1) NOT NULL DEFAULT '1',
  `Descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programadelealtadrecompensa_change`
--

DROP TABLE IF EXISTS `programadelealtadrecompensa_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programadelealtadrecompensa_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PuntosRequeridos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Activa` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `programadelealtadrecompensa_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promocion`
--

DROP TABLE IF EXISTS `promocion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ProductosAOtorgar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductosAOtorgarTo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `VigenciaHasta` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DiasHorario` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `DiasSemana` int(11) NOT NULL DEFAULT '0',
  `Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `VigenciaDesde` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TipoDeVigencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SiempreVigente',
  `Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `CombinarProductos` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promocion_change`
--

DROP TABLE IF EXISTS `promocion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductosAOtorgar` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductosAOtorgarTo` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_VigenciaHasta` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Porcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DiasHorario` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DiasSemana` int(11) NOT NULL DEFAULT '0',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VigenciaDesde` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_TipoDeVigencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CombinarProductos` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `promocion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promocionproducto`
--

DROP TABLE IF EXISTS `promocionproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocionproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Promocion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Producto` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PromocionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PromocionID` (`PromocionID`),
  KEY `IX_ProductoID` (`ProductoID`),
  CONSTRAINT `promocionproducto_ibfk_1` FOREIGN KEY (`PromocionID`) REFERENCES `promocion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `promocionproducto_ibfk_2` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promocionproducto_change`
--

DROP TABLE IF EXISTS `promocionproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocionproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PromocionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `promocionproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Calle` varchar(255) DEFAULT '',
  `Nombre` varchar(255) DEFAULT '',
  `Referencia1` varchar(255) DEFAULT '',
  `RFC` varchar(255) DEFAULT '',
  `NombreFiscal` varchar(255) DEFAULT '',
  `CP` varchar(255) DEFAULT '',
  `Colonia` varchar(255) DEFAULT '',
  `PaginaWeb` varchar(255) DEFAULT '',
  `Pais` varchar(255) DEFAULT '',
  `Numero` varchar(255) DEFAULT '',
  `DelegacionMunicipio` varchar(255) DEFAULT '',
  `Estado` varchar(255) DEFAULT '',
  `Telefono1` varchar(255) DEFAULT '',
  `Ciudad` varchar(255) DEFAULT '',
  `Telefono2` varchar(255) DEFAULT '',
  `Email` varchar(255) DEFAULT '',
  `Fax` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CP` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RFC` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PaginaWeb` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Pais` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Numero` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DelegacionMunicipio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NombreFiscal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Calle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Telefono1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Telefono2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Fax` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ContactoNombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proveedores_change`
--

DROP TABLE IF EXISTS `proveedores_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CP` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RFC` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Colonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PaginaWeb` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Pais` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Numero` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DelegacionMunicipio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreFiscal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ContactoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Calle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Telefono1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Telefono2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `proveedores_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recurringtask`
--

DROP TABLE IF EXISTS `recurringtask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurringtask` (
  `Identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Schedule` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Statements` longtext,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `RunAsID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Name` (`Name`),
  KEY `IX_RunAsID` (`RunAsID`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `recurringtask_ibfk_1` FOREIGN KEY (`RunAsID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `recurringtask_ibfk_2` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recurringtask_change`
--

DROP TABLE IF EXISTS `recurringtask_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurringtask_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Schedule` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RunAsID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Code` longtext COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `recurringtask_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seccionsucursal`
--

DROP TABLE IF EXISTS `seccionsucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seccionsucursal` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SucursalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SucursalID` (`SucursalID`),
  CONSTRAINT `seccionsucursal_ibfk_1` FOREIGN KEY (`SucursalID`) REFERENCES `sucursal` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seccionsucursal_change`
--

DROP TABLE IF EXISTS `seccionsucursal_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seccionsucursal_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SucursalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `seccionsucursal_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seriefactura`
--

DROP TABLE IF EXISTS `seriefactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seriefactura` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `FolioSiguiente` bigint(20) NOT NULL DEFAULT '0',
  `Serie` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seriefactura_change`
--

DROP TABLE IF EXISTS `seriefactura_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seriefactura_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FolioSiguiente` bigint(20) NOT NULL DEFAULT '0',
  `Obj_Serie` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `seriefactura_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `SerieFacturaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Calle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CP` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Telefono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Numero` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DelegacionMunicipio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Notas` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `GiroID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Pais` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `EmpresaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ContactoNombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ContactoTelefono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ContactoEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `UbicacionDomicilio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `EmailNotificarCortesDeCaja` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioOtraColonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioCalle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioCodigoPostal` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioNumeroExterior` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioColonia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioDelegacionMunicipio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioEstado` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Distrito Federal',
  `DomicilioNumeroInterior` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioPais` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'México ',
  `DomicilioCiudad` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DomicilioInformacionAdicional` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_SerieFacturaID` (`SerieFacturaID`),
  KEY `IX_GiroID` (`GiroID`),
  KEY `IX_EmpresaID` (`EmpresaID`),
  KEY `IX_DomicilioCodigoPostal` (`DomicilioCodigoPostal`),
  CONSTRAINT `sucursal_ibfk_1` FOREIGN KEY (`SerieFacturaID`) REFERENCES `seriefactura` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `sucursal_ibfk_2` FOREIGN KEY (`GiroID`) REFERENCES `girodenegocio` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `sucursal_ibfk_3` FOREIGN KEY (`EmpresaID`) REFERENCES `empresa` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sucursal_change`
--

DROP TABLE IF EXISTS `sucursal_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EmpresaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioOtraColonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ContactoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SerieFacturaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioCalle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioCodigoPostal` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ContactoTelefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioNumeroExterior` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioColonia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ContactoEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UbicacionDomicilio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioDelegacionMunicipio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioEstado` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioNumeroInterior` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioPais` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_EmailNotificarCortesDeCaja` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioCiudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Notas` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DomicilioInformacionAdicional` longtext COLLATE utf8_unicode_ci,
  `Obj_GiroID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `sucursal_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemsettings`
--

DROP TABLE IF EXISTS `systemsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemsettings` (
  `AppName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `AppLogo` longtext COLLATE utf8_unicode_ci,
  `AppSmallLogo` longtext COLLATE utf8_unicode_ci,
  `AppIcon` longtext COLLATE utf8_unicode_ci,
  `GoogleMapsAPIKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPPort` int(11) NOT NULL DEFAULT '587',
  `SMTPUseSsl` tinyint(1) NOT NULL DEFAULT '1',
  `SMTPFromEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPFromName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPReplyTo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SMTPHeaders` longtext COLLATE utf8_unicode_ci,
  `LogLevel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Information',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemsettings_change`
--

DROP TABLE IF EXISTS `systemsettings_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemsettings_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AppName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AppLogo` longtext COLLATE utf8_unicode_ci,
  `Obj_AppSmallLogo` longtext COLLATE utf8_unicode_ci,
  `Obj_AppIcon` longtext COLLATE utf8_unicode_ci,
  `Obj_GoogleMapsAPIKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPPort` int(11) NOT NULL DEFAULT '0',
  `Obj_SMTPUseSsl` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_SMTPFromEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPFromName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPReplyTo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SMTPHeaders` longtext COLLATE utf8_unicode_ci,
  `Obj_LogLevel` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `systemsettings_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_cuentabancaria`
--

DROP TABLE IF EXISTS `t_cuentabancaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_cuentabancaria` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ID_Cuenta` varchar(255) DEFAULT '',
  `t_cuentabancaria` varchar(255) DEFAULT '',
  `Nombre` varchar(255) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_movimientoinventario`
--

DROP TABLE IF EXISTS `t_movimientoinventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_movimientoinventario` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia` longtext,
  `NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProveedorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Notas` longtext,
  `CuentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_ProveedorID` (`ProveedorID`),
  KEY `IX_CuentaID` (`CuentaID`),
  CONSTRAINT `t_movimientoinventario_ibfk_1` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `t_movimientoinventario_ibfk_2` FOREIGN KEY (`ProveedorID`) REFERENCES `proveedores` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `t_movimientoinventario_ibfk_3` FOREIGN KEY (`CuentaID`) REFERENCES `cuentabancaria` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tarjeta`
--

DROP TABLE IF EXISTS `tarjeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjeta` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Activa` tinyint(1) NOT NULL DEFAULT '1',
  `PIN` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Numero` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PermitirComprarCualquierProducto` tinyint(1) NOT NULL DEFAULT '1',
  `LimitePorDia` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Foto` longblob,
  `Apellidos` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LimitePorMes` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `LimitePorSemana` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FotoFileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Numero` (`Numero`),
  KEY `IX_Numero` (`Numero`),
  KEY `IX_Nombre` (`Nombre`),
  KEY `IX_Apellidos` (`Apellidos`),
  KEY `IX_ClienteID` (`ClienteID`),
  KEY `IX_PIN` (`PIN`),
  CONSTRAINT `tarjeta_ibfk_1` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tarjeta_change`
--

DROP TABLE IF EXISTS `tarjeta_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjeta_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Activa` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_PIN` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Numero` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PermitirComprarCualquierProducto` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_LimitePorDia` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Foto` longblob,
  `Obj_FotoFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Apellidos` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LimitePorMes` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LimitePorSemana` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `tarjeta_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tarjetaproductopermitido`
--

DROP TABLE IF EXISTS `tarjetaproductopermitido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjetaproductopermitido` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Producto` varchar(255) DEFAULT '',
  `TarjetaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_TarjetaID` (`TarjetaID`),
  KEY `IX_ProductoID` (`ProductoID`),
  CONSTRAINT `tarjetaproductopermitido_ibfk_1` FOREIGN KEY (`TarjetaID`) REFERENCES `tarjeta` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `tarjetaproductopermitido_ibfk_2` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tarjetaproductopermitido_change`
--

DROP TABLE IF EXISTS `tarjetaproductopermitido_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjetaproductopermitido_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TarjetaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `tarjetaproductopermitido_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal`
--

DROP TABLE IF EXISTS `terminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `SucursalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroDeTerminal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clave` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CopiarProductosID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ConfiguracionID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `AlmacenID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Codigo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'CajaGenerica',
  PRIMARY KEY (`ID`),
  KEY `IX_SucursalID` (`SucursalID`),
  KEY `IX_CopiarProductosID` (`CopiarProductosID`),
  KEY `IX_ConfiguracionID` (`ConfiguracionID`),
  KEY `IX_AlmacenID` (`AlmacenID`),
  CONSTRAINT `terminal_ibfk_1` FOREIGN KEY (`SucursalID`) REFERENCES `sucursal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `terminal_ibfk_2` FOREIGN KEY (`CopiarProductosID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `terminal_ibfk_3` FOREIGN KEY (`ConfiguracionID`) REFERENCES `configuraciondecaja` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `terminal_ibfk_4` FOREIGN KEY (`AlmacenID`) REFERENCES `almacen` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal_change`
--

DROP TABLE IF EXISTS `terminal_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clave` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ConfiguracionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AlmacenID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SucursalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `terminal_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminalcatalogodeproductos`
--

DROP TABLE IF EXISTS `terminalcatalogodeproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminalcatalogodeproductos` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CatalogoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_CatalogoID` (`CatalogoID`),
  CONSTRAINT `terminalcatalogodeproductos_ibfk_1` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `terminalcatalogodeproductos_ibfk_2` FOREIGN KEY (`CatalogoID`) REFERENCES `catalogodeproductos` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminalcatalogodeproductos_change`
--

DROP TABLE IF EXISTS `terminalcatalogodeproductos_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminalcatalogodeproductos_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CatalogoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `terminalcatalogodeproductos_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminalfoliosecuencial`
--

DROP TABLE IF EXISTS `terminalfoliosecuencial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminalfoliosecuencial` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Venta',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FolioSiguiente` bigint(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_FolioSiguiente` (`FolioSiguiente`),
  CONSTRAINT `terminalfoliosecuencial_ibfk_1` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminalfoliosecuencial_change`
--

DROP TABLE IF EXISTS `terminalfoliosecuencial_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminalfoliosecuencial_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FolioSiguiente` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `terminalfoliosecuencial_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipodecliente`
--

DROP TABLE IF EXISTS `tipodecliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodecliente` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `LongitudMinimaDeIdentificadorFiscal` int(11) NOT NULL DEFAULT '0',
  `IdentificadorFiscalDefault` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'RFC',
  `CobrarImpuestoDeVenta3` tinyint(1) NOT NULL DEFAULT '1',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ValidarLongitudDeIdentificadorFiscal` tinyint(1) NOT NULL DEFAULT '1',
  `LongitudMaximaDeIdentificadorFiscal` int(11) NOT NULL DEFAULT '0',
  `PermitirModificarIdentificadorFiscal` tinyint(1) NOT NULL DEFAULT '1',
  `CobrarImpuestoDeVenta1` tinyint(1) NOT NULL DEFAULT '1',
  `CobrarImpuestoDeVenta2` tinyint(1) NOT NULL DEFAULT '1',
  `LongitudDeIdentificadorFiscal` int(11) NOT NULL DEFAULT '0',
  `LongitudDeIdentificadorFiscalTo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipodecliente_change`
--

DROP TABLE IF EXISTS `tipodecliente_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodecliente_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_IdentificadorFiscalDefault` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CobrarImpuestoDeVenta3` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ValidarLongitudDeIdentificadorFiscal` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_PermitirModificarIdentificadorFiscal` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CobrarImpuestoDeVenta1` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_LongitudDeIdentificadorFiscal` int(11) NOT NULL DEFAULT '0',
  `Obj_LongitudDeIdentificadorFiscalTo` int(11) NOT NULL DEFAULT '0',
  `Obj_CobrarImpuestoDeVenta2` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `tipodecliente_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipodedireccion`
--

DROP TABLE IF EXISTS `tipodedireccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodedireccion` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipodedireccion_change`
--

DROP TABLE IF EXISTS `tipodedireccion_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodedireccion_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `tipodedireccion_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipodeproducto`
--

DROP TABLE IF EXISTS `tipodeproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodeproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `NombreInformacionVenta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Venta',
  `MostrarInformacionGeneral` tinyint(1) NOT NULL DEFAULT '0',
  `NombreModificadores` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Modificadores',
  `NombreInformacionCompra` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Compra',
  `NombreInformacionGeneral` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Información General',
  `NombreConversiones` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Conversiones',
  `MostrarInformacionCompra` tinyint(1) NOT NULL DEFAULT '1',
  `EsSubproducto` tinyint(1) NOT NULL DEFAULT '0',
  `MostrarInformacionVenta` tinyint(1) NOT NULL DEFAULT '1',
  `MostrarModificadores` tinyint(1) NOT NULL DEFAULT '1',
  `NombreAlmacenes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Almacenes',
  `MostrarCompuestoPor` tinyint(1) NOT NULL DEFAULT '1',
  `MostrarConversiones` tinyint(1) NOT NULL DEFAULT '1',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NombreCompuestoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Compuesto Por',
  `MostrarAlmacenes` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipodeproducto_change`
--

DROP TABLE IF EXISTS `tipodeproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodeproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreInformacionVenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MostrarAlmacenes` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_NombreModificadores` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreInformacionCompra` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreInformacionGeneral` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreConversiones` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MostrarInformacionCompra` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_EsSubproducto` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_MostrarInformacionVenta` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_MostrarModificadores` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_NombreAlmacenes` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MostrarCompuestoPor` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_MostrarConversiones` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreCompuestoPor` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `tipodeproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipomovimientoinventario`
--

DROP TABLE IF EXISTS `tipomovimientoinventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipomovimientoinventario` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaccionprepago`
--

DROP TABLE IF EXISTS `transaccionprepago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaccionprepago` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Referencia2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SaldoAnterior` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Tipo` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Monto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDeTarjeta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Folio` bigint(20) NOT NULL AUTO_INCREMENT,
  `TarjetaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `PIN` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Folio` (`Folio`),
  KEY `IX_Tipo` (`Tipo`),
  KEY `IX_ClienteID` (`ClienteID`),
  KEY `IX_Folio` (`Folio`),
  KEY `IX_TarjetaID` (`TarjetaID`),
  KEY `IX_Fecha` (`Fecha`),
  CONSTRAINT `transaccionprepago_ibfk_1` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `transaccionprepago_ibfk_2` FOREIGN KEY (`TarjetaID`) REFERENCES `tarjeta` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaccionprepago_change`
--

DROP TABLE IF EXISTS `transaccionprepago_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaccionprepago_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SaldoAnterior` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Tipo` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Monto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDeTarjeta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PIN` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia3` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Folio` bigint(20) NOT NULL DEFAULT '0',
  `Obj_TarjetaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `transaccionprepago_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trustedcertificateauthority`
--

DROP TABLE IF EXISTS `trustedcertificateauthority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trustedcertificateauthority` (
  `StoreName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Certificate` longblob,
  `CertificateFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext COLLATE utf8_unicode_ci,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `CertificateIssuer` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `CertificateSubject` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `CertificateSerialNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `CertificateThumbprint` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `CertificateNotBefore` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `CertificateNotAfter` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  PRIMARY KEY (`ID`),
  KEY `IX_StoreName` (`StoreName`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  KEY `IX_CertificateIssuer` (`CertificateIssuer`),
  KEY `IX_CertificateSubject` (`CertificateSubject`),
  KEY `IX_CertificateSerialNumber` (`CertificateSerialNumber`),
  KEY `IX_CertificateThumbprint` (`CertificateThumbprint`),
  KEY `IX_CertificateNotBefore` (`CertificateNotBefore`),
  KEY `IX_CertificateNotAfter` (`CertificateNotAfter`),
  CONSTRAINT `trustedcertificateauthority_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trustedcertificateauthority_change`
--

DROP TABLE IF EXISTS `trustedcertificateauthority_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trustedcertificateauthority_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_StoreName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Certificate` longblob,
  `Obj_CertificateFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificateIssuer` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificateSubject` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificateSerialNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificateThumbprint` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CertificateNotBefore` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_CertificateNotAfter` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `trustedcertificateauthority_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `LimiteEntrada` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Comentario` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LimiteSalida` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'NoEspecificado',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turno_change`
--

DROP TABLE IF EXISTS `turno_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LimiteEntrada` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Comentario` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LimiteSalida` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `turno_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turnoterminal`
--

DROP TABLE IF EXISTS `turnoterminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turnoterminal` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegistradoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaCierre` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `FechaApertura` datetime(6) NOT NULL DEFAULT '2000-01-01 00:00:00.000000',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CerradoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `AutorizadoPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Cerrado` tinyint(1) NOT NULL DEFAULT '0',
  `RegistradoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `AutorizadoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_RegistradoPorID` (`RegistradoPorID`),
  KEY `IX_AutorizadoPorID` (`AutorizadoPorID`),
  CONSTRAINT `turnoterminal_ibfk_1` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `turnoterminal_ibfk_2` FOREIGN KEY (`RegistradoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `turnoterminal_ibfk_3` FOREIGN KEY (`AutorizadoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turnoterminal_change`
--

DROP TABLE IF EXISTS `turnoterminal_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turnoterminal_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RegistradoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaCierre` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_CerradoPor` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AutorizadoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cerrado` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_FechaApertura` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `turnoterminal_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uicomponent`
--

DROP TABLE IF EXISTS `uicomponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uicomponent` (
  `Identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Name` varchar(255) DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Body` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Default` tinyint(1) NOT NULL DEFAULT '0',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `AutomaticallyGenerated` tinyint(1) NOT NULL DEFAULT '0',
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_Name` (`Name`),
  KEY `IX_Default` (`Default`),
  KEY `IX_Enabled` (`Enabled`),
  KEY `IX_AutomaticallyGenerated` (`AutomaticallyGenerated`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `uicomponent_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uicomponent_change`
--

DROP TABLE IF EXISTS `uicomponent_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uicomponent_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Body` longtext COLLATE utf8_unicode_ci,
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AutomaticallyGenerated` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `uicomponent_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12335 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uifile`
--

DROP TABLE IF EXISTS `uifile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uifile` (
  `Identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ContentType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'application/javascript',
  `Minify` tinyint(1) NOT NULL DEFAULT '1',
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `uifile_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uifile_change`
--

DROP TABLE IF EXISTS `uifile_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uifile_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ContentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Minify` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Content` longtext COLLATE utf8_unicode_ci,
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `uifile_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uifilecache`
--

DROP TABLE IF EXISTS `uifilecache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uifilecache` (
  `CreationDate` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `ContentType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Response` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uipage`
--

DROP TABLE IF EXISTS `uipage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uipage` (
  `Identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `IsDefault` tinyint(1) NOT NULL DEFAULT '0',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Identifier` (`Identifier`),
  KEY `IX_Identifier` (`Identifier`),
  KEY `IX_Name` (`Name`),
  KEY `IX_IsDefault` (`IsDefault`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `uipage_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uipage_change`
--

DROP TABLE IF EXISTS `uipage_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uipage_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Content` longtext COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `uipage_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uipagepartial`
--

DROP TABLE IF EXISTS `uipagepartial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uipagepartial` (
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Description` longtext COLLATE utf8_unicode_ci,
  `Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Content` longtext COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`),
  KEY `IX_Name` (`Name`),
  KEY `IX_Tags` (`Tags`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `uipagepartial_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uipagepartial_change`
--

DROP TABLE IF EXISTS `uipagepartial_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uipagepartial_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Description` longtext COLLATE utf8_unicode_ci,
  `Obj_Tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Content` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `uipagepartial_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidad`
--

DROP TABLE IF EXISTS `unidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidad` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidad_change`
--

DROP TABLE IF EXISTS `unidad_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidad_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `unidad_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidadequivalencia`
--

DROP TABLE IF EXISTS `unidadequivalencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidadequivalencia` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Unidad2ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CantidadEquivalencia` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Unidad1ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_Unidad2ID` (`Unidad2ID`),
  KEY `IX_Unidad1ID` (`Unidad1ID`),
  CONSTRAINT `unidadequivalencia_ibfk_1` FOREIGN KEY (`Unidad2ID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `unidadequivalencia_ibfk_2` FOREIGN KEY (`Unidad1ID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidadequivalencia_change`
--

DROP TABLE IF EXISTS `unidadequivalencia_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidadequivalencia_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Unidad2ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CantidadEquivalencia` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Unidad1ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `unidadequivalencia_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidadequivalencias`
--

DROP TABLE IF EXISTS `unidadequivalencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidadequivalencias` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Unidad2ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `CantidadEquivalencia` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Unidad1ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_Unidad2ID` (`Unidad2ID`),
  KEY `IX_Unidad1ID` (`Unidad1ID`),
  CONSTRAINT `unidadequivalencias_ibfk_1` FOREIGN KEY (`Unidad2ID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `unidadequivalencias_ibfk_2` FOREIGN KEY (`Unidad1ID`) REFERENCES `unidad` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `updateschemarequest`
--

DROP TABLE IF EXISTS `updateschemarequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updateschemarequest` (
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `RequestedByID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Status` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `StatusDetails` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Statements` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_Date` (`Date`),
  KEY `IX_RequestedByID` (`RequestedByID`),
  KEY `IX_Status` (`Status`),
  CONSTRAINT `updateschemarequest_ibfk_1` FOREIGN KEY (`RequestedByID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `updateschemarequest_change`
--

DROP TABLE IF EXISTS `updateschemarequest_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updateschemarequest_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `OLD_Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `NEW_Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `OLD_RequestedByID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NEW_RequestedByID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `OLD_Status` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NEW_Status` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `OLD_StatusDetails` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `NEW_StatusDetails` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `OLD_Statements` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `NEW_Statements` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `OLD_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NEW_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `OLD_PersistorRowVersion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NEW_PersistorRowVersion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_RequestedByID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Status` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_StatusDetails` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Obj_Statements` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Obj_ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_OLD_ID` (`OLD_ID`),
  KEY `IX_NEW_ID` (`NEW_ID`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `updateschemarequest_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7324 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `FirstName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `LastName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `UserRoleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Protected` tinyint(1) NOT NULL DEFAULT '0',
  `DBUsername` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DBPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Type` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'User',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `DBUserSetupPending` tinyint(1) NOT NULL DEFAULT '1',
  `TimeZone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'UTC',
  `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ClaveDeAutorizacion` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PermitirAsignarSoloRolesSeleccionados` tinyint(1) NOT NULL DEFAULT '0',
  `NumeroDeEmpleado` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SucursalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Rfc` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Curp` varchar(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoRegimen` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroDeSeguroSocial` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Banco` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Departamento` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CLABE` varchar(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Puesto` varchar(170) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoDeJornada` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `RiesgoPuesto` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PerioricidadDePago` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Quincenal',
  `TipoDeContrato` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `CP` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Referencia` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `SalarioBase` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `SalarioDiarioIntegrado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Clasificacion3ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ClaveAutorizacionAsistencia` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Clasificacion2ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia1` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Clasificacion1ID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia3` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Referencia2` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `NumeroExterior` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Colonia` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Municipio` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Calle` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroInterior` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Estado` varchar(130) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Pais` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Ciudad` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TelefonoCasa` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaDeNacimiento` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `TelefonoOficina` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TipoSexo` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Masculino',
  `TelefonoMovil` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaIngreso` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `HorarioID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `HuellaDigital2` longblob,
  `HuellaDigital2FileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `NombreContactoDeEmergencia` varchar(170) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `TelefonoContactoDeEmergencia` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `HuellaDigital1` longblob,
  `HuellaDigital1FileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Fotografia` longblob,
  `FotografiaFileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PeriodicidadDePago` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Quincenal',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`),
  KEY `IX_Username` (`Username`),
  KEY `IX_Password` (`Password`),
  KEY `IX_UserRoleID` (`UserRoleID`),
  KEY `IX_DBUsername` (`DBUsername`),
  KEY `IX_Type` (`Type`),
  KEY `IX_DBUserSetupPending` (`DBUserSetupPending`),
  KEY `IX_Email` (`Email`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  KEY `IX_SucursalID` (`SucursalID`),
  KEY `IX_Clasificacion3ID` (`Clasificacion3ID`),
  KEY `IX_Clasificacion2ID` (`Clasificacion2ID`),
  KEY `IX_Clasificacion1ID` (`Clasificacion1ID`),
  KEY `IX_HorarioID` (`HorarioID`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`UserRoleID`) REFERENCES `userrole` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`SucursalID`) REFERENCES `sucursal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_4` FOREIGN KEY (`Clasificacion3ID`) REFERENCES `clasificacionempleado3` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_5` FOREIGN KEY (`Clasificacion2ID`) REFERENCES `clasificacionempleado2` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_6` FOREIGN KEY (`Clasificacion1ID`) REFERENCES `clasificacionempleado1` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_7` FOREIGN KEY (`HorarioID`) REFERENCES `horario` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_change`
--

DROP TABLE IF EXISTS `user_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Type` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FirstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_LastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Username` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TimeZone` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UserRoleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_DBUsername` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DBPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DBUserSetupPending` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia1` longtext COLLATE utf8_unicode_ci,
  `Obj_SucursalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CP` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TelefonoCasa` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeSeguroSocial` varchar(13) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_HorarioID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_HuellaDigital2` longblob,
  `Obj_HuellaDigital2FileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeJornada` varchar(35) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaDeNacimiento` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_SalarioDiarioIntegrado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ClaveDeAutorizacion` varchar(70) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clasificacion1ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clasificacion3ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_RiesgoPuesto` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Rfc` varchar(13) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Estado` varchar(130) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NombreContactoDeEmergencia` varchar(170) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroExterior` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Banco` varchar(5) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia` longtext COLLATE utf8_unicode_ci,
  `Obj_Colonia` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Municipio` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Calle` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Pais` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Departamento` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia3` longtext COLLATE utf8_unicode_ci,
  `Obj_PermitirAsignarSoloRolesSeleccionados` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_TelefonoOficina` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoSexo` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeEmpleado` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Curp` varchar(18) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TelefonoContactoDeEmergencia` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_CLABE` varchar(18) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Ciudad` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TelefonoMovil` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaIngreso` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Referencia2` longtext COLLATE utf8_unicode_ci,
  `Obj_TipoRegimen` varchar(5) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_HuellaDigital1` longblob,
  `Obj_HuellaDigital1FileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClaveAutorizacionAsistencia` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Clasificacion2ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroInterior` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Puesto` varchar(170) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PeriodicidadDePago` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeContrato` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SalarioBase` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Fotografia` longblob,
  `Obj_FotografiaFileName` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `user_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrole` (
  `Name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `AllowAll` tinyint(1) NOT NULL DEFAULT '0',
  `Protected` tinyint(1) NOT NULL DEFAULT '0',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `UpdateDbSchema` tinyint(1) NOT NULL DEFAULT '0',
  `SendEmail` tinyint(1) NOT NULL DEFAULT '1',
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`),
  KEY `IX_Name` (`Name`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `userrole_ibfk_1` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrole_change`
--

DROP TABLE IF EXISTS `userrole_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrole_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AllowAll` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `userrole_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrolepermission`
--

DROP TABLE IF EXISTS `userrolepermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrolepermission` (
  `UserRoleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DataTypeID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `AllowCreate` tinyint(1) NOT NULL DEFAULT '0',
  `AllowRead` tinyint(1) NOT NULL DEFAULT '0',
  `AllowUpdate` tinyint(1) NOT NULL DEFAULT '0',
  `AllowDelete` tinyint(1) NOT NULL DEFAULT '0',
  `AllowReadChanges` tinyint(1) NOT NULL DEFAULT '0',
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `ObjectData` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ReadFilter` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `UpdateFilter` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `DeleteFilter` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ReadChangesFilter` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `InstalledModuleID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_UserRoleID` (`UserRoleID`),
  KEY `IX_DataTypeID` (`DataTypeID`),
  KEY `IX_InstalledModuleID` (`InstalledModuleID`),
  CONSTRAINT `userrolepermission_ibfk_1` FOREIGN KEY (`UserRoleID`) REFERENCES `userrole` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `userrolepermission_ibfk_2` FOREIGN KEY (`DataTypeID`) REFERENCES `datatype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `userrolepermission_ibfk_3` FOREIGN KEY (`InstalledModuleID`) REFERENCES `installedmodule` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrolepermission_change`
--

DROP TABLE IF EXISTS `userrolepermission_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrolepermission_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_UserRoleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DataTypeID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_AllowCreate` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AllowRead` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AllowUpdate` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AllowDelete` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_AllowReadChanges` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_ObjectData` longtext COLLATE utf8_unicode_ci,
  `Obj_ReadFilter` longtext COLLATE utf8_unicode_ci,
  `Obj_UpdateFilter` longtext COLLATE utf8_unicode_ci,
  `Obj_DeleteFilter` longtext COLLATE utf8_unicode_ci,
  `Obj_ReadChangesFilter` longtext COLLATE utf8_unicode_ci,
  `Obj_InstalledModuleID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `userrolepermission_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userx`
--

DROP TABLE IF EXISTS `userx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userx` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `RegistradaPor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PedidoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Notas` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Cambio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CancelacionAutorizadaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `FolioUnico` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto2PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MotivoCancelacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DescuentoPorcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Facturada` tinyint(1) NOT NULL DEFAULT '0',
  `SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TurnoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `NumeroDeTerminal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Propina` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FacturaNotificada` tinyint(1) NOT NULL DEFAULT '0',
  `Referencia4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `SeparadorFolioDeVenta` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `FechaInicioFacturacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `NumeroDePersonas` int(11) NOT NULL DEFAULT '0',
  `Referencia1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoSubtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Referencia3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CanceladaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Referencia5` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `VendedorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Folio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Impuesto3PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `TotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FolioSecuencial` bigint(20) NOT NULL DEFAULT '0',
  `Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MontoPagado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Referencia2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `Cancelada` tinyint(1) NOT NULL DEFAULT '0',
  `CorteDeCajaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `RegistradaPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DireccionDeEnvioID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `ClienteID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `TransferenciaTerminalID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoAutorizadoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PedidoID` (`PedidoID`),
  KEY `IX_TerminalID` (`TerminalID`),
  KEY `IX_TurnoID` (`TurnoID`),
  KEY `IX_CorteDeCajaID` (`CorteDeCajaID`),
  KEY `IX_RegistradaPorID` (`RegistradaPorID`),
  KEY `IX_CancelacionAutorizadaPorID` (`CancelacionAutorizadaPorID`),
  KEY `IX_CanceladaPorID` (`CanceladaPorID`),
  KEY `IX_VendedorID` (`VendedorID`),
  KEY `IX_DireccionDeEnvioID` (`DireccionDeEnvioID`),
  KEY `IX_ClienteID` (`ClienteID`),
  KEY `IX_TransferenciaTerminalID` (`TransferenciaTerminalID`),
  KEY `IX_DescuentoAutorizadoPorID` (`DescuentoAutorizadoPorID`),
  KEY `IX_DescuentoID` (`DescuentoID`),
  CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`PedidoID`) REFERENCES `pedido` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_10` FOREIGN KEY (`CanceladaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_11` FOREIGN KEY (`VendedorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_12` FOREIGN KEY (`DireccionDeEnvioID`) REFERENCES `clientedireccion` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_13` FOREIGN KEY (`ClienteID`) REFERENCES `cliente` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_14` FOREIGN KEY (`TransferenciaTerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_15` FOREIGN KEY (`DescuentoAutorizadoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_16` FOREIGN KEY (`DescuentoID`) REFERENCES `descuento` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_3` FOREIGN KEY (`TerminalID`) REFERENCES `terminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_4` FOREIGN KEY (`TurnoID`) REFERENCES `turnoterminal` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_7` FOREIGN KEY (`CorteDeCajaID`) REFERENCES `cortedecaja` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_8` FOREIGN KEY (`RegistradaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_9` FOREIGN KEY (`CancelacionAutorizadaPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venta_change`
--

DROP TABLE IF EXISTS `venta_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_RegistradaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PedidoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Notas` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cambio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CancelacionAutorizadaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FolioUnico` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoTotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto2PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MotivoCancelacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoPorcentaje` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Facturada` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_SubtotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TurnoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Propina` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FacturaNotificada` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_Referencia4` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_SeparadorFolioDeVenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Subtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Fecha` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_FechaInicioFacturacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Total` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_NumeroDePersonas` int(11) NOT NULL DEFAULT '0',
  `Obj_Referencia1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoSubtotal` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Referencia3` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaCancelacion` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_DescuentoImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto1PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CanceladaPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeArticulos` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Impuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Referencia5` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Impuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_VendedorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Folio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FechaRegistro` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Obj_Impuesto3PrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_TotalPrecioDeLista` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FolioSecuencial` bigint(20) NOT NULL DEFAULT '0',
  `Obj_Impuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MontoPagado` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_Referencia2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Cancelada` tinyint(1) NOT NULL DEFAULT '0',
  `Obj_CorteDeCajaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DireccionDeEnvioID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ClienteID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TransferenciaTerminalID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoAutorizadoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `venta_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ventapago`
--

DROP TABLE IF EXISTS `ventapago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventapago` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `FormaDePagoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `NumeroDeCuenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PropinaMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `VentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `MontoPagadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PropinaMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `MontoPagadoMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `FormaDePagoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `Referencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `TipoDeCambio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  KEY `IX_VentaID` (`VentaID`),
  KEY `IX_FormaDePagoID` (`FormaDePagoID`),
  CONSTRAINT `ventapago_ibfk_1` FOREIGN KEY (`VentaID`) REFERENCES `venta` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ventapago_ibfk_2` FOREIGN KEY (`FormaDePagoID`) REFERENCES `formadepago` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ventapago_change`
--

DROP TABLE IF EXISTS `ventapago_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventapago_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_FormaDePagoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_NumeroDeCuenta` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PropinaMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_VentaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_MontoPagadoMonedaExtranjera` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PropinaMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_MontoPagadoMonedaNacional` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_FormaDePagoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_Referencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_TipoDeCambio` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `ventapago_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ventaproducto`
--

DROP TABLE IF EXISTS `ventaproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventaproducto` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `DescuentoUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVentaUnitario2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeVentaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoCodigo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `ProductoCantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoNombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `PorcentajeImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CatalogoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PrecioDeListaUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVentaUnitario3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeVentaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `CantidadDevuelta` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PrecioDeListaUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ImpuestoDeVentaUnitario1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `PorcentajeImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `ProductoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `VentaID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `DescuentoUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `DescuentoReferencia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `DescuentoAutorizadoPorID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CatalogoID` (`CatalogoID`),
  KEY `IX_ProductoID` (`ProductoID`),
  KEY `IX_VentaID` (`VentaID`),
  KEY `IX_DescuentoID` (`DescuentoID`),
  KEY `IX_DescuentoAutorizadoPorID` (`DescuentoAutorizadoPorID`),
  CONSTRAINT `ventaproducto_ibfk_1` FOREIGN KEY (`CatalogoID`) REFERENCES `catalogodeproductos` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ventaproducto_ibfk_3` FOREIGN KEY (`ProductoID`) REFERENCES `producto` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ventaproducto_ibfk_4` FOREIGN KEY (`VentaID`) REFERENCES `venta` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ventaproducto_ibfk_5` FOREIGN KEY (`DescuentoID`) REFERENCES `descuento` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ventaproducto_ibfk_6` FOREIGN KEY (`DescuentoAutorizadoPorID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ventaproducto_change`
--

DROP TABLE IF EXISTS `ventaproducto_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventaproducto_change` (
  `ID` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `PersistorRowVersion` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `Date` datetime(6) NOT NULL DEFAULT '2000-01-01 01:01:01.000000',
  `Order` bigint(20) NOT NULL AUTO_INCREMENT,
  `Action` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByDBUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `PerformedByUserID` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `PerformedFromHost` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PersistorRowVersion` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVentaUnitario2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeVentaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoCodigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_ProductoCantidad` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoNombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PorcentajeImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioConImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CatalogoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_PrecioDeListaUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVentaUnitario3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoUnitarioImpuesto2` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeVentaUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_CantidadDevuelta` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PrecioDeListaUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoUnitarioImpuesto1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ImpuestoDeVentaUnitario1` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_PorcentajeImpuesto3` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_ProductoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_VentaID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoUnitarioSinImpuesto` decimal(26,10) NOT NULL DEFAULT '0.0000000000',
  `Obj_DescuentoReferencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `Obj_DescuentoAutorizadoPorID` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`Order`),
  KEY `IX_Date` (`Date`),
  KEY `IX_Order` (`Order`),
  KEY `IX_Action` (`Action`),
  KEY `IX_PerformedByDBUser` (`PerformedByDBUser`),
  KEY `IX_PerformedByUserID` (`PerformedByUserID`),
  KEY `IX_PerformedFromHost` (`PerformedFromHost`),
  KEY `IX_Obj_ID` (`Obj_ID`),
  CONSTRAINT `ventaproducto_change_ibfk_1` FOREIGN KEY (`PerformedByUserID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-10 13:31:28
