import { Component, ViewChild } from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
   @ViewChild(Nav) nav: Nav;
  rootPage:any = 'LoginEmailPage';
pages: Array<{title: string, component: any, icon: any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private network: Network) {
    
    
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available
     
     

      this.pages = [
        { title: 'Guardar Venta', component: 'Bebidas', icon: 'floppy-o' },
        { title: 'Cancelar Venta', component: 'Dulces', icon: 'times-circle'},
        { title: 'Abnr/Cerrar Caja', component: 'scissors' ,icon: 'cut' },
        { title: 'Asignar Cliente', component: 'Comida', icon: 'user-plus' },
        { title: 'Asignar Vendedor', component: 'nieves', icon: 'black-tie' },
        { title: 'Retiro/Deposito', component: 'nieves', icon: 'university' },
        { title: 'Devolucicon', component: 'Licores dulces', icon: 'minus-square-o' },
        { title: 'Asignar Puntos', component: 'Licores dulces', icon: 'address-card-o' },
        { title: 'Facturar', component: 'Licores dulces', icon: 'file-text-o' },
        { title: 'Abnr Cajon', component: 'Licores dulces', icon: 'clipboard' },
        { title: 'Sincronizacion', component: 'Licores dulces', icon: 'refresh' },
        { title: 'Desconectar', component: 'Licores dulces', icon: 'lock' }
    
     
    ];
    

    


      statusBar.styleDefault();
      splashScreen.hide() ;
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }



  
}

