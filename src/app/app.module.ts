import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TodosProvider } from '../providers/todos/todos';
import { Network } from '@ionic-native/network';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { HttpProvider } from '../providers/http/http';
import { HttpModule } from '@angular/http';
import { FilterArrayPipe } from '../pipes/order-by/order-by';
import { Pipe } from "@angular/core";
import { DataProvider } from '../providers/data/data';
import { CategoryProvider } from '../providers/category/category';
import { ProductProvider } from '../providers/product/product';
import { RegisterCustomerProvider } from '../providers/register-customer/register-customer';

//import { Ng2OrderModule } from 'ng2-order-pipe';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    
  FilterArrayPipe,
  
   // Pipe
 
    
  ],
  imports: [
    BrowserModule,
     HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    //Pipe
  ],
  providers: [
    StatusBar,
    SplashScreen,
  
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    HttpProvider,
    TodosProvider,
    Network,
   FilterArrayPipe,
    DataProvider,
    CategoryProvider,
    ProductProvider,
    RegisterCustomerProvider,
  
   
   // Pipe,
  
  ]
})
export class AppModule {




}
