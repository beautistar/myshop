import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Platform} from 'ionic-angular';
import {HttpProvider} from '../../providers/http/http';
import { Network } from '@ionic-native/network';
import { TodosProvider } from '../../providers/todos/todos';
import { DataProvider } from '../../providers/data/data';
import { CategoryProvider } from '../../providers/category/category';
import { ProductProvider } from '../../providers/product/product';
import { Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
  providers:[HttpProvider,TodosProvider, CategoryProvider, ProductProvider],
 
})

export class MenuPage {
  @ViewChild(Slides) slides: Slides;
  Nombre: any;
  count = 1 ;
  ttl1=0;
  myDate: String = new Date().toISOString();
  side_up_icon: Array<{ component: any, icon: any}>;
  side_menu : Array<{ number: number, Component: any; prize: any}>;
  side_menu1 : Array<{ number: number, Component: any; prize: any}>;
  bill_ttl : Array<{ component: any, prize: number}>;
  newsData :any;
   news :any;
  newsData1 : any;  
  newsData_length:any;
  sub_main_menu : any;
  sub_main_menu1 : any;
  sub_main_menu_filter :any;
  ttl : any;
  selet_item:any;
  num : any;
  com : any;
  priz : any;
  items = [];
  id: any;
  sub_tot:number;
  itemName:any;
  ttl1r:any;
  i : any;
  todos : any ;
  isTabletOrIpad : any ;
  higt : any;
  sys_higt : any;
  fnt_size : any;
  bot_height : any;
  scrl_hght : any;
constructor(public plt: Platform, public navCtrl: NavController, public navParams: NavParams,  private httpProvider:HttpProvider , public todoService: TodosProvider, public dataService: DataProvider, public categoryService: CategoryProvider, public ProductService: ProductProvider, private network: Network) 
{ 

  plt.ready().then((readySource) => {
    this.sys_higt =  plt.height();
    this.higt = (this.sys_higt)+"px";

   if((this.sys_higt) <= 900   )
    {
      this.fnt_size = 11+"px";
      this.bot_height = 45+"px";
     //this.sc;rl_hght = 47+"px"
    }
    
  });


  this.side_menu = [];
  //-------left_upper_icon_array
  this.side_up_icon = [
      { component: 'Bebidas', icon: 'plus' },
      { component: 'scissors' ,icon: 'minus' },
      {component: 'Comida', icon: 'remove'},
      {component: 'nieves', icon: 'list-alt'},
      {component: 'nieves', icon: 'comment' } 
    ];
//-----------left_side_bill_total
  this.bill_ttl = [
           {component : 'Subtotall', prize : 0},
           {component : 'Decsuento', prize : 0.02},
           {component : 'Impuestos', prize : 0}
       ];
//------------------code_for_bill_total
  this.ttl =0;
  for(var j=0 ;j<(this.bill_ttl.length); j++ )
    {
    
       this.ttl = this.ttl+this.bill_ttl[j].prize;
    }


    console.log('chat');
    this.autoScroll();
   
}


/* For Auto Scroll */

autoScroll() {
  setTimeout(function () {
      var itemList = document.getElementById("scrol_product");
      itemList.scrollTop = itemList.scrollHeight;

  }, 10);
}

/*Autro scrooll end  */



//------------------code_for_bill_total_when_click_a_product
get_sub_tot = function ()
{

  this.sub_tot=0;
  this.ttl1r=0;
  for(var j=0 ;j<(this.side_menu.length); j++ )
    {
    
       this.sub_tot+=parseFloat(this.side_menu[j].prize);
       this.ttl1r+=0.16;
    }
      this.bill_ttl[0].prize= this.sub_tot.toFixed(2);;
      this.bill_ttl[2].prize=this.ttl1r.toFixed(2);
      this.ttl=(parseFloat(this.bill_ttl[0].prize)+parseFloat(this.bill_ttl[1].prize)+parseFloat(this.bill_ttl[2].prize)).toFixed(2);
  }
  
//----------code_for_add_product_in_side_navigation
addItem = function (itemName, itemPrice) 
{ 
     //alert(itemName+" add  "+itemPrice);
      /*  this.side_menu.push({    
      Component: itemName,
      number : 1,
      prize:Math.ceil(itemPrice),
     });  */
     this.autoScroll();
    this.todoService.createTodo({  Component: itemName,
      number : 1,
      prize: itemPrice.toFixed(2)
    });
     
       this.todoService.getTodos().then((res) =>{
            this.side_menu=  res ;
            }); 
    itemName = "";
    this.get_sub_tot();
    console.log("Success_data_of_category : "+itemName); 
}
  //addItem("Renu");
  st = function(itemPrice){
    let sub=0;
    let total=sub+itemPrice;
  }
//------------------end_code_for_bill_total

  ionViewDidLoad() {
//--------------get_data_from_pouchdb_for_category



//---------get_data_from_pouchdb_for_product
     /* this.sub_main_menu1 = [];
      this.ProductService.getTodos().then((res) =>{
          this.sub_main_menu1 = res ;
          //alert(JSON.stringify(this.sub_main_menu1)) ;
          }); 
*/

//---------get_data_from_pouchdb_for_side_selecting_product  
    this.todoService.getTodos().then((res) =>{
              this.side_menu = res ;
              this.get_sub_tot();
         }); 
   
// ------------code for category_list_fetching_from_server
 this.httpProvider.main_product().subscribe(
    result => { 
      
      this.news= result;
    
      this.newsData_length=this.news.length;
    //  alert( this.newsData_length);
     this.categoryService.getTodos().then((res) =>{
     this.newsData = res ;
    if((this.news)==0)
      {
            for(let i =0 ; i<this.newsData_length; i++)
              {
                this.categoryService.createTodo({ Nombre : this.news[i].Nombre ,
                id : this.news[i].ID
                   });    
              }
               
      }
    });
 
     


      //alert(this.newsData[0].ID);
 
    /*  for(let i =0 ; i<this.newsData_length; i++)
          {
                //this.newsData1 = this.newsData;
                  {
                    alert(this.newsData[i]);
                      this.categoryService.createTodo({ Nombre : this.newsData[i].Nombre ,
                        id : this.newsData[i].ID
                      });
                   }
              }
     */    
    },
    err =>{

      this.categoryService.getTodos().then((res) =>{
      
        this.news = res;
        this.newsData_length=this.news.length;
      });
     
    
    
    } ,
    () => {
      console.log('caegory_getData completed');
    });

// ------------code for product_list_fetching_from_server
 this.httpProvider.sub_main_product().subscribe(
    result => { 
    this.sub_main_menu1 = result;

     //alert(JSON.stringify(this.sub_main_menu1));

   /*for(let i =0 ; i<(this.sub_main_menu.length); i++)
                {
                  //this.newsData1 = this.newsData;
                    {
                      //alert(this.sub_main_menu[i].Nombre);
                    // alert(this.sub_main_menu[i].PrecioSinImpuesto);
                          this.ProductService.createTodo({ Nombre : this.sub_main_menu[i].Nombre ,
                          PrecioSinImpuesto : this.sub_main_menu[i].PrecioSinImpuesto ,
                          CategoriaID : this.sub_main_menu[i].CategoriaID

                        });
                      }
                  }
      */

this.ProductService.getTodos().then((res) =>{
     this.sub_main_menu = res ;
    if((this.sub_main_menu)==0)
      {
               for(let i =0 ; i<(this.sub_main_menu1.length); i++)
              {
                
              this.ProductService.createTodo({ Nombre : this.sub_main_menu1[i].Nombre ,
                PrecioSinImpuesto : this.sub_main_menu1[i].PrecioSinImpuesto ,
                          CategoriaID : this.sub_main_menu1[i].CategoriaID

                        });  
              }         
      }
   
      } );
     // alert(JSON.stringify(this.sub_main_menu));
    },
    err =>{
      console.error("Error_of_category : "+err);
      this.ProductService.getTodos().then((res) =>{
        this.sub_main_menu1 = res ;
        });
    } ,
    () => {
      console.log('caegory_getData completed');
    });

}

 show_corre_menu(item)
  {
 for(var j=0 ;j<(this.side_menu.length); j++ )
    {
        if( (this.side_menu[j].Component) == item )
        {
          this.num = this.side_menu[j].number;
          this.com = this.side_menu[j].Component;
          this.priz = this.side_menu[j].prize;
        }
    }
  
  }


 

  openPage(ev)
  {

  }
  getItems(ev)
  {

  }
  
  show_search()
  {
    this.i = !this.i;
    
  }
  show_categ(id){

     //alert(JSON.stringify(this.sub_main_menu1[0].Nombre)) ;
    // alert(id);
    this.sub_main_menu_filter=this.sub_main_menu1.filter(function(data){

        return  data.CategoriaID == id  ;
        

    });
      console.log(JSON.stringify(this.sub_main_menu_filter)+ "************************fil");
  }
//----------- swipe_slide_next
  nextSlide(){
    
    this.slides.slideNext();    
  }
//------------swipe_slide_prev
  prevslide()
  {
  this.slides.slidePrev();
  }
}





