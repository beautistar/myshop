import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpProvider} from '../../providers/http/http';
import { RegisterCustomerProvider } from '../../providers/register-customer/register-customer';
/**
 * Generated class for the LoginEmailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-email',
  templateUrl: 'login-email.html',
  providers:[HttpProvider , RegisterCustomerProvider]
})
export class LoginEmailPage {
  username : any;
  password : any;
  unm : any;
  pwd : any;
  login_data : any;
  check_login : any;
  pdb_login_data : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private httpProvider:HttpProvider,  public loginService: RegisterCustomerProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginEmailPage');
  }

  gotoKeypadLogin() {    
      this.navCtrl.setRoot('LoginKeypadPage');    
  }

  
   //----------check_login
  
  //--------------------function_for_chec_login
  gotomainLogin(ud,pd)
  {
    this.httpProvider.check_login().subscribe(
      result =>
      { 
        this.login_data = result  ;
        this.unm = this.login_data.filter(function(data){
          return  (data.Username == ud && data.Password == pd)  ;
            });
               
          //alert(JSON.stringify(this.unm));
      //----------code_for_enter_data_in_puchdb  
       if((this.unm) !=0)
        {  
          this.loginService.getTodos().then((res) =>{
          this.pdb_login_data = res ;
          if((this.pdb_login_data)==0)
          {
            for(let i =0 ; i<(this.login_data.length); i++)
            {
              this.loginService.createTodo({ Username : this.login_data[i].Username ,
                Password : this.login_data[i].Password
              });    
            }
                      
          }
            });
      //----------end_of_check_login_code
          this.navCtrl.setRoot('MenuPage');
        }
          else{
            alert("You Enter Wrong Password");
          }
      
       },
       err =>
       {
          console.log("Error in login"+err);
          this.loginService.getTodos().then((res) =>
          {
            this.login_data = res; 
            this.unm = this.login_data.filter(function(data){
              
                    return  (data.Username == ud && data.Password == pd)  ;
                  });
                     
            if((this.unm) !=0)
            {
              this.navCtrl.setRoot('MenuPage');
            }
            else{
                alert("You Enter Wrong Password");
            }
          });
       },

     () => {
      console.log('caegory_getData completed');
      });
    //this.navCtrl.setRoot('MenuPage');
  }
}
