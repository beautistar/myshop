import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginKeypadPage } from './login-keypad';
import { IonPasscodeComponent } from '../../components/ion-passcode/ion-passcode';

@NgModule({
  declarations: [
    LoginKeypadPage,
    IonPasscodeComponent
  ],
  imports: [
    IonicPageModule.forChild(LoginKeypadPage),
  ],
  exports: [
    LoginKeypadPage
  ]
})
export class LoginKeypadPageModule {}
