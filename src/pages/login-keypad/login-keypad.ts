import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IonPasscodeOptions } from '../../components/ion-passcode/ion-passcode';

/**
 * Generated class for the LoginKeypadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-keypad',
  templateUrl: 'login-keypad.html',
})
export class LoginKeypadPage {

  passcodeOptions: IonPasscodeOptions;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initPasscodeView();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginKeypadPage');
    
  }

  initPasscodeView() {
    console.log('initPasscodeView LoginKeypadPage');
    let that = this
    this.passcodeOptions = {
      title: 'PIN',
      length: 8,
      onComplete: function(passcode: string) {
        // you have to return a promise once you have verified the passcode
        // if the passcode is invalid call `reject()`
        // if the passcode is valid call `resolve()`
        return new Promise((resolve, reject) => {
          if (passcode == '00000000') {  // Here is correct passcode

            // ToDo: Add success block

            resolve();
          }
          else {  // Here is exception 


            reject();
          }
        });
      },
      onGotoLogin: function() {
        return new Promise((resolve, reject) => {
          that.navCtrl.setRoot('LoginEmailPage')
          resolve();
        });
      }
    }
  }

  gotoEmailLogin() {    
      this.navCtrl.setRoot('LoginEmailPage');    
  }

}
