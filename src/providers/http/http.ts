import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class HttpProvider {

  constructor(public http: Http) {
    console.log('Hello HttpProvider Provider');

  }

 main_product()
  
      {
        let header = new Headers();
      
        header.append('Accept', 'application/json');
        header.append('Authorization',  'Basic ZGV2OnFCN1FpWW1mUzBHZW5zVFp0eTFM');
        let options = new RequestOptions({ headers: header });
        
          return  this.http.get('https://dev.mysoftware.mx/api/data/categoriadeproducto', options).map(res => res.json());
        
      }
  sub_main_product()
  
      {
        let header = new Headers();
      
        header.append('Accept', 'application/json');
        header.append('Authorization',  'Basic ZGV2OnFCN1FpWW1mUzBHZW5zVFp0eTFM');
        let options = new RequestOptions({ headers: header });
       return  this.http.get('https://dev.mysoftware.mx/api/data/producto', options).map(res => res.json());
        
        
      }
//-------check_login
    check_login()
    {
      
      let header = new Headers();
      
        header.append('Accept', 'application/json');
        header.append('Authorization',  'Basic ZGV2OnFCN1FpWW1mUzBHZW5zVFp0eTFM');
        let options = new RequestOptions({ headers: header });
       return  this.http.get('https://dev.mysoftware.mx/api/data/user', options).map(res => res.json());
       
    }

}
