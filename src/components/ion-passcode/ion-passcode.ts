import { Component, Input,ViewChild } from '@angular/core';

/**
 * Generated class for the IonPasscodeComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'ion-passcode',
  templateUrl: 'ion-passcode.html'
})
export class IonPasscodeComponent {
  @Input() options: IonPasscodeOptions;
@ViewChild('focusInput') myInput ;
  public passcode: string = '';
  public defaultMaxLength: number = 4;
  public hasError: boolean = false;

  constructor() {  }
ionViewLoaded() {

    setTimeout(() => {
     
      this.myInput.setFocus();
    },150); //a least 150ms.

 }
  ngOnInit() {
    if (this.options === undefined || this.options === null) {
      console.error('[IonPasscode] options are not defined.');
      return;
    }
  }

  onDeleteKeyClick() {
    this.passcode = this.passcode.substr(0, this.passcode.length - 1);
  }

  onKeyClick(key: number) {
    if (this.passcode.length < this.getPassCodeMaxLength() && !this.hasError) {
      this.passcode += key;
     
    }
    if (this.passcode.length == this.getPassCodeMaxLength() && this.options.onComplete) {
      setTimeout(() => {
        let promise = this.options.onComplete(this.passcode);
        if (promise) {
          promise.then(() => {
            this.passcode = '';
          }, () => {
            this.passcode = '';
            this.hasError = true;
            setTimeout(() => { this.hasError = false; }, 1000);
          });
        }
      }, 250);
    }
  }

  getDeleteKeyValue() {
    return this.options.deleteKeyValue ? this.options.deleteKeyValue : 'DELETE';
  }

  getDeleteKeyType() {
    this.options.deleteKeyType = 'icon';
    return this.options.deleteKeyType === 'icon' ? 'icon' : 'text';
  }

  getPassCodeMaxLength() {
    return this.options.length > 0 ? this.options.length : this.defaultMaxLength;
  }

  getIndicatorArray() {
    let i = 0;
    return Array(this.getPassCodeMaxLength()).fill(1).map((x) => { i = i + 1; return i; });
  }

  gotoEmailLogin() {    
      if (this.options.onGotoLogin) {
      setTimeout(() => {
        let promise = this.options.onGotoLogin();
        if (promise) {
          promise.then(() => {
            this.passcode = '';
          }, () => {
            this.passcode = '';
            this.hasError = true;
            setTimeout(() => { this.hasError = false; }, 1000);
          });
        }
      }, 250);
    }
  }
}

export interface IonPasscodeOptions {
  title: string,
  length?: number,
  deleteKeyValue?: string,
  deleteKeyType?: string,
  onComplete?: Function,
  onGotoLogin?: Function,
}
