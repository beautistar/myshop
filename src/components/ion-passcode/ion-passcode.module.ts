import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonPasscodeComponent } from './ion-passcode';

@NgModule({
  declarations: [
    IonPasscodeComponent,
  ],
  imports: [
    IonicPageModule.forChild(IonPasscodeComponent),
  ],
  exports: [
    IonPasscodeComponent
  ]
})
export class IonPasscodeComponentModule {}
